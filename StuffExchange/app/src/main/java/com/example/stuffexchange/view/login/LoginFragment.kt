package com.example.stuffexchange.view.login

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.activity.addCallback
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.example.stuffexchange.R
import com.example.stuffexchange.model.NotificaOggetto
import com.example.stuffexchange.model.Utente
import com.example.stuffexchange.util.Caricamento
import com.example.stuffexchange.util.Connessione
import com.example.stuffexchange.util.Notifiche
import com.example.stuffexchange.view.MainActivity
import com.example.stuffexchange.viewmodel.LoginViewModel
import com.example.stuffexchange.viewmodel.ViewModelFactory
import kotlinx.android.synthetic.main.fragment_login.*
import kotlinx.android.synthetic.main.fragment_login.email
import kotlinx.android.synthetic.main.nav_header.*


class LoginFragment : Fragment() {

    private lateinit var viewModelFactory: ViewModelFactory
    private lateinit var viewModel: LoginViewModel
    private lateinit var caricamento : Caricamento
    private lateinit var connessione : Connessione
    private lateinit var notifica : Notifiche
    private lateinit var appCompatActivity: AppCompatActivity
    private lateinit var mainActivity: MainActivity

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        mainActivity = requireActivity() as MainActivity
        viewModelFactory = ViewModelFactory(mainActivity.application)
        viewModel = ViewModelProvider(this, viewModelFactory).get(LoginViewModel::class.java)
        caricamento = Caricamento(requireActivity())
        connessione = Connessione(requireActivity())
        notifica = Notifiche(requireActivity())
        appCompatActivity = requireActivity() as AppCompatActivity
        mainActivity.setDrawerLocked(true)
        return inflater.inflate(R.layout.fragment_login, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        login_btn.setOnClickListener {
            login()
        }

        forgotPass_btn.setOnClickListener {
            resetPassword()
        }

        signUp_btn.setOnClickListener {
            val navController = findNavController()

            if(navController.currentDestination?.id == R.id.loginFragment)
                navController.navigate(R.id.action_loginFragment_to_registrazioneFragment)
        }

        requireActivity().onBackPressedDispatcher.addCallback(this){
            val intent = Intent(Intent.ACTION_MAIN)
            intent.addCategory(Intent.CATEGORY_HOME)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
            startActivity(intent)
        }
    }

    override fun onResume() {
        super.onResume()
        appCompatActivity.supportActionBar?.hide()
        osservaCaricamento()
        osservaReset()
        osservaLogin()
        osservaAggiornamentoUtente()
        osservaControlloOggettiScaduti()

        if(!viewModel.getCaricamento().value!!)
            viewModel.logout()
    }


    override fun onStop() {
        super.onStop()
        caricamento.dismissDialog()
    }

    /*----- LOGIN -----*/

    private fun login() {
        if(!connessione.controlloConnessione()){
            Toast.makeText(requireActivity(), "Connessione assente", Toast.LENGTH_SHORT).show()
            return
        }

        if(!viewModel.controlloDatiLogin(email, password))
            return

        viewModel.login(email, password).observe(viewLifecycleOwner, Observer { eventWrapper ->
            val result = eventWrapper.getContentIfNotHandled()
            azioniLogin(result)
        })
    }

    private fun osservaLogin() {
        viewModel.getSuccessLogin()?.observe(viewLifecycleOwner, Observer { eventWrapper ->
            val result = eventWrapper.getContentIfNotHandled()
            azioniLogin(result)
        })
    }

    private fun azioniLogin(result: Boolean?) {
        result?.let {
            if(it) {
                Toast.makeText(requireActivity(), "Login effettuato con successo", Toast.LENGTH_SHORT).show()
                aggiornamentoDatiUtente()
            }
            else {
                Toast.makeText(requireActivity(), "Errore durante il login", Toast.LENGTH_SHORT).show()
                viewModel.setCaricamento(false)
            }
        }
    }

    private fun aggiornamentoDatiUtente() {
        viewModel.aggiornamentoDatiUtente()?.observe(viewLifecycleOwner, Observer { eventWrapper ->
            val result = eventWrapper.getContentIfNotHandled()
            azioniAggiornamentoUtente(result)
        })
    }

    private fun osservaAggiornamentoUtente() {
        viewModel.getsuccessAggiornamentoUtente()?.observe(viewLifecycleOwner, Observer {eventWrapper ->
            val result = eventWrapper.getContentIfNotHandled()
            azioniAggiornamentoUtente(result)
        })
    }

    private fun azioniAggiornamentoUtente(result: Utente?) {
        result?.let {
            if(it.nome==""){
                viewModel.logout()
                viewModel.setCaricamento(false)
                Toast.makeText(requireActivity(), "Errore durante l'aggiornamento dei dati utente, riprovare il login", Toast.LENGTH_SHORT).show()
            }

            mainActivity.aggiornamentoDrawer(it.nome, it.cognome, it.mail, it.votoMedio, it.numeroVoti)
            viewModel.setCaricamento(false)
            Toast.makeText(requireActivity(), "Dati utente aggiornati", Toast.LENGTH_SHORT).show()
            controllaOggettiScaduti()
        }
    }

    private fun controllaOggettiScaduti(){
        viewModel.controllaOggettiScaduti().observe(viewLifecycleOwner, Observer {eventWrapper->
            val result = eventWrapper.getContentIfNotHandled()
            azioniControlloOggettiScaduti(result)
        })
    }

    private fun osservaControlloOggettiScaduti() {
        viewModel.getControlloOggettiScaduti()?.observe(viewLifecycleOwner, Observer { eventWrapper ->
            if(eventWrapper == null) {
                Toast.makeText(mainActivity, "Non è stato possibile controllare gli oggetti scaduti, ti invitiamo a controllare eventuali scadenze", Toast.LENGTH_SHORT).show()
                viewModel.setCaricamento(false)
                mainActivity.setDrawerLocked(false)
                val navController = findNavController()

                if(navController.currentDestination?.id == R.id.loginFragment)
                navController.navigate(R.id.action_loginFragment_to_searchFragment)
            }

            val result = eventWrapper.getContentIfNotHandled()
            azioniControlloOggettiScaduti(result)
        })
    }

    private fun azioniControlloOggettiScaduti(result: NotificaOggetto?) {
        result?.let {
            notifica.setupNotification()

            if(it.caricato){
                notifica.sendNotification("Controlla la lista degli oggetti caricati",1)
            }

            if(it.prestato){
                notifica.sendNotification("Controlla la lista degli oggetti prestati", 2)
            }

            if(it.inPrestito){
                notifica.sendNotification("Controlla la lista degli oggetti presi in prestito", 3)
            }

            viewModel.setCaricamento(false)
            mainActivity.setDrawerLocked(false)
            val navController = findNavController()

            if(navController.currentDestination?.id == R.id.loginFragment)
                navController.navigate(R.id.action_loginFragment_to_searchFragment)
        }
    }

    /*----- RESET PASSWORD -----*/

    private fun resetPassword() {
        if(!connessione.controlloConnessione()){
            Toast.makeText(requireActivity(), "Connessione assente", Toast.LENGTH_SHORT).show()
            return
        }

        if(!viewModel.controlloDatiResetPassword(email))
            return

        viewModel.resetPassword(email).observe(viewLifecycleOwner, Observer {eventWrapper ->
            val result = eventWrapper.getContentIfNotHandled()
            azioniReset(result)
        })
    }

    private fun osservaReset() {
        viewModel.getSuccessReset()?.observe(viewLifecycleOwner, Observer {eventWrapper ->
            val result = eventWrapper.getContentIfNotHandled()
            azioniReset(result)
        })
    }

    private fun azioniReset(result: Boolean?) {
        result?.let {
            if (it)
                Toast.makeText(requireActivity(), "Controlla la mail per reimpostare la password", Toast.LENGTH_SHORT).show()
            else
                Toast.makeText(requireActivity(), "Email non trovata", Toast.LENGTH_SHORT).show()

            viewModel.setCaricamento(false)
        }
    }

    /*----- CARICAMENTO -----*/
    private fun osservaCaricamento() {
        viewModel.getCaricamento().observe(viewLifecycleOwner, Observer {
            if(it)
                caricamento.loadingAlertDialog()
            else
                caricamento.dismissDialog()
        })
    }
}
