package com.example.stuffexchange.repository

import android.app.Application
import android.os.AsyncTask
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.stuffexchange.data.FirebaseDatabase
import com.example.stuffexchange.data.LocalDatabase
import com.example.stuffexchange.model.EventWrapper
import com.example.stuffexchange.model.Oggetto
import com.example.stuffexchange.model.OggettoAttivo
import com.example.stuffexchange.model.Utente
import com.google.android.gms.tasks.OnFailureListener
import com.google.android.gms.tasks.OnSuccessListener
import com.google.firebase.firestore.DocumentSnapshot
import com.google.firebase.firestore.QuerySnapshot

class OggettiRepository(application: Application){
    private val firebaseDatabase : FirebaseDatabase = FirebaseDatabase.instance
    private  var localeDatabase: LocalDatabase = LocalDatabase.getInstance(application)

    fun scaricaListaOgettiCaricati(): LiveData<ArrayList<Oggetto>> {
        val listaOggetti = MutableLiveData<ArrayList<Oggetto>>()

        val oggettiCaricatiSuccessListener  = OnSuccessListener<QuerySnapshot>{ it ->
            val oggetti = ArrayList<Oggetto>()

            it.forEach {
                oggetti.add(it.toObject(Oggetto::class.java))
            }

            listaOggetti.value = oggetti
        }

        val oggettiCaricatiFailureListener = OnFailureListener{
            listaOggetti.value = null
        }

        firebaseDatabase.scaricaListaOggettiCaricati(oggettiCaricatiSuccessListener, oggettiCaricatiFailureListener)
        return listaOggetti
    }

    fun eliminaOggetto(idOggetto: String): LiveData<EventWrapper<Boolean>> {

        val eliminaSuccess = MutableLiveData<EventWrapper<Boolean>>()

        val eliminaOggettoSuccessListener = OnSuccessListener<Void>{
            eliminaSuccess.value = EventWrapper(true)
        }

        val eliminaOggettoFailureListener = OnFailureListener{
            eliminaSuccess.value = EventWrapper(false)
        }

        firebaseDatabase.eliminaOggetto(idOggetto, eliminaOggettoSuccessListener, eliminaOggettoFailureListener)
        return eliminaSuccess
    }

    fun aggiornaListaInPrestito(): LiveData<ArrayList<OggettoAttivo>> {
        val listaOggettiPresiInPrestito = MutableLiveData<ArrayList<OggettoAttivo>>()

        val oggettiPresiInPrestitoSuccessListener = OnSuccessListener<QuerySnapshot>{

            it?.let {listaOggetti->
                val aux = ArrayList<OggettoAttivo>()

                listaOggetti.forEach {oggetto->
                    aux.add(oggetto.toObject(OggettoAttivo::class.java))
                }

                listaOggettiPresiInPrestito.value = aux
            }
        }

        val oggettiPresiInPrestitoFailureListener = OnFailureListener {
            listaOggettiPresiInPrestito.value = null
        }

        firebaseDatabase.aggiornaListaInPrestito(oggettiPresiInPrestitoSuccessListener, oggettiPresiInPrestitoFailureListener)
        return listaOggettiPresiInPrestito

    }

    fun aggiornaListaPrestati(): LiveData<ArrayList<OggettoAttivo>> {
        val listaOggettiPrestati = MutableLiveData<ArrayList<OggettoAttivo>>()

        val oggettiPrestatiSuccessListener = OnSuccessListener<QuerySnapshot> {

            it?.let {listaOggetti->
                val aux = ArrayList<OggettoAttivo>()

                listaOggetti.forEach { oggetto->
                    aux.add(oggetto.toObject(OggettoAttivo::class.java))
                }

                listaOggettiPrestati.value = aux
            }
        }

        val oggettiPrestatiFailureListener = OnFailureListener {
            listaOggettiPrestati.value = null
        }

        firebaseDatabase.aggiornaListaPrestati(oggettiPrestatiSuccessListener, oggettiPrestatiFailureListener)
        return listaOggettiPrestati
    }

    fun recensioneUtente(
        rating: Float,
        oggettoAttivo: OggettoAttivo,
        codiceDestinazione: Int
    ): LiveData<EventWrapper<Boolean>> {
        val success = MutableLiveData<EventWrapper<Boolean>>()

        val recensioneSuccessListener = OnSuccessListener<Void>{
            success.value = EventWrapper(true)
        }

        val recensioneFailureListener = OnFailureListener {
            success.value = EventWrapper(false)
        }

        val aggiornamentoSuccessListener = OnSuccessListener<DocumentSnapshot>{documento->
            val utente = documento?.toObject(Utente::class.java)

            utente?.let {
                val numeroVoti = utente.numeroVoti
                val votoMedio = utente.votoMedio
                val totaleVoti = votoMedio*numeroVoti
                val nuovoNumeroVoti = numeroVoti+1
                val nuovoVotoMedio = (totaleVoti+rating)/nuovoNumeroVoti

                RecensioneUtenteTask(
                    localeDatabase,
                    firebaseDatabase,
                    recensioneSuccessListener,
                    recensioneFailureListener,
                    nuovoVotoMedio,
                    nuovoNumeroVoti,
                    oggettoAttivo,
                    codiceDestinazione).execute()
            }
        }

        /*Recurera il documdento dell'utente loggato */
        firebaseDatabase.recensioneUtente(aggiornamentoSuccessListener, recensioneFailureListener, oggettoAttivo)
        return success
    }

    /*Aggiorna recensione nel databse locale*/
    class RecensioneUtenteTask(
        private val db: LocalDatabase,
        private val database: FirebaseDatabase,
        private val recensioneSuccessListener: OnSuccessListener<Void>,
        private val recensioneFailureListener: OnFailureListener,
        private val nuovoVotoMedio: Float,
        private val nuovoNumeroVoti: Int,
        private val oggettoAttivo: OggettoAttivo,
        private val codiceDestinazione: Int
    ): AsyncTask<Int, Void, Void>() {
        override fun doInBackground(vararg params: Int?): Void? {
            db.getUtenteDAO().updateRecensione(nuovoNumeroVoti, nuovoVotoMedio)
            return null
        }

        override fun onPostExecute(result: Void?) {
            super.onPostExecute(result)

            /*aggiorna la recensione in Firebase*/
            database.aggiornaRecensione(
                nuovoNumeroVoti,
                nuovoVotoMedio,
                recensioneSuccessListener,
                recensioneFailureListener,
                oggettoAttivo,
                codiceDestinazione)
        }
    }
}