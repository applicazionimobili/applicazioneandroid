package com.example.stuffexchange.repository

import android.app.Application
import android.os.AsyncTask
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.stuffexchange.data.FirebaseDatabase
import com.example.stuffexchange.data.LocalDatabase
import com.example.stuffexchange.model.EventWrapper
import com.example.stuffexchange.model.Oggetto
import com.example.stuffexchange.model.OggettoAttivo
import com.example.stuffexchange.model.Utente
import com.google.android.gms.tasks.OnFailureListener
import com.google.android.gms.tasks.OnSuccessListener
import com.google.firebase.firestore.QuerySnapshot

class MappaRepository(application: Application) {
    private val firebaseDatabase: FirebaseDatabase = FirebaseDatabase.instance
    private var localeDatabase: LocalDatabase = LocalDatabase.getInstance(application)

    fun prenotaOggetto(
        oggettoAttivo: OggettoAttivo,
        idVenditore: String?
    ): LiveData<EventWrapper<Boolean>> {
        val success = MutableLiveData<EventWrapper<Boolean>>()

        val prenotaOggettoSuccessListener = OnSuccessListener<Void>{
            success.value = EventWrapper(true)
        }

        val prenotaOggettoFailureListener = OnFailureListener{
            success.value = EventWrapper(false)
        }

        GetUtenteTask(localeDatabase,firebaseDatabase, oggettoAttivo, idVenditore, prenotaOggettoSuccessListener, prenotaOggettoFailureListener).execute()

        return success
    }

    class GetUtenteTask(
        private val db: LocalDatabase,
        private val database: FirebaseDatabase,
        private val oggettoAttivo: OggettoAttivo,
        private val idVenditore: String?,
        private val prenotaOggettoSuccessListener: OnSuccessListener<Void>,
        private val prenotaOggettoFailureListener: OnFailureListener
    ): AsyncTask<Void, Void, Utente>() {
        override fun doInBackground(vararg params: Void?): Utente {
            return db.getUtenteDAO().getUtente()
        }

        override fun onPostExecute(result: Utente?) {
            super.onPostExecute(result)
            val compratore = result

            compratore?.let {
                database.prenotaOggetto(oggettoAttivo, compratore, idVenditore!!, prenotaOggettoSuccessListener, prenotaOggettoFailureListener)
            }
        }
    }

    fun aggiornateDatePrenotate(oggeto: Oggetto?): LiveData<ArrayList<String>> {
        val datePrenotate = MutableLiveData<ArrayList<String>>()

        /*Fra gli oggetti prestati selezioniamo solo l'oggetto desiderato e inseriamo nell'array datePrenotate
        * tutti i periodi prenotate */
        val datePrenotateSuccessListener = OnSuccessListener<QuerySnapshot>{listaOggetti->
            val listaAux = ArrayList<String>()

            listaOggetti?.forEach {
                val oggettoAttivo = it.toObject(OggettoAttivo::class.java)
                if(oggettoAttivo.idOggetto!!.contains(oggeto?.id!!)){
                    val aux = "${oggettoAttivo.inizioPrestito}-${oggettoAttivo.finePrestito}"
                    listaAux.add(aux)
                }
            }

            datePrenotate.value = listaAux
        }

        val datePrenotateFailureListener = OnFailureListener {
            datePrenotate.value = null
        }

        /*Restituisce la lista degli oggetti prestati*/
        firebaseDatabase.aggiornaDatePrenotate(oggeto!!, datePrenotateSuccessListener, datePrenotateFailureListener)
        return datePrenotate
    }
}