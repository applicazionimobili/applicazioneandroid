package com.example.stuffexchange.view.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.Navigation
import androidx.recyclerview.widget.RecyclerView
import com.example.stuffexchange.R
import com.example.stuffexchange.model.Oggetto
import com.example.stuffexchange.view.list.OggettiCaricatiFragmentDirections
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.oggetto_caricato.view.*

class OggettiCaricatiAdapter(val listaOggetti: ArrayList<Oggetto>) : RecyclerView.Adapter<OggettiCaricatiAdapter.ObjectViewHolder>() {

    inner class ObjectViewHolder(var view: View) : RecyclerView.ViewHolder(view) {}

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ObjectViewHolder {

        val inflater = LayoutInflater.from(parent.context)
        return if(viewType == 1){
            val view = inflater.inflate(R.layout.oggetto_caricato, parent, false)
            ObjectViewHolder((view))
        } else{
            val view = inflater.inflate(R.layout.oggetto_caricato_scaduto, parent, false)
            ObjectViewHolder((view))
        }
    }

    override fun getItemCount(): Int = listaOggetti.size

    override fun onBindViewHolder(holder: ObjectViewHolder, position: Int) {
        holder.view.nome.text = listaOggetti[position].nome
        holder.view.startDate.text = listaOggetti[position].startData
        holder.view.endDate.text = listaOggetti[position].endData
        Picasso.get().load(listaOggetti[position].imgUrl).fit().placeholder(R.drawable.caricamento).into(holder.view.imageView)

        holder.view.setOnClickListener {
            val action = OggettiCaricatiFragmentDirections.actionOggettiCaricatiFragmentToOggettiCaricatiDettagliFragment(listaOggetti[position])
            Navigation.findNavController(it).navigate(action)
        }
    }


    fun aggiornaLista(newObjectLIst: List<Oggetto>) {
        listaOggetti.clear()
        listaOggetti.addAll(newObjectLIst)
        notifyDataSetChanged()
    }

    override fun getItemViewType(position: Int): Int {
        return if(listaOggetti[position].stato == "in progress")
            1
        else
            2
    }
}