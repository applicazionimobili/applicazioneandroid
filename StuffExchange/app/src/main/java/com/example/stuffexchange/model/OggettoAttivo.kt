package com.example.stuffexchange.model

import android.os.Parcel
import android.os.Parcelable

data class OggettoAttivo(
    var idUtente: String?,
    var nomeUtente: String?,
    var idOggetto: String?,
    var imgUrl: String?,
    var nome: String?,
    var posizione: String?,
    var inizioPrestito: String?,
    var finePrestito: String?,
    var stato: String?): Parcelable {

    constructor(parcel: Parcel) : this(
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString()
    ) {
    }

    constructor(): this("", "","", "", "", "", "", "", "")

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(idUtente)
        parcel.writeString(nomeUtente)
        parcel.writeString(idOggetto)
        parcel.writeString(imgUrl)
        parcel.writeString(nome)
        parcel.writeString(posizione)
        parcel.writeString(inizioPrestito)
        parcel.writeString(finePrestito)
        parcel.writeString(stato)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<OggettoAttivo> {
        override fun createFromParcel(parcel: Parcel): OggettoAttivo {
            return OggettoAttivo(parcel)
        }

        override fun newArray(size: Int): Array<OggettoAttivo?> {
            return arrayOfNulls(size)
        }
    }
}