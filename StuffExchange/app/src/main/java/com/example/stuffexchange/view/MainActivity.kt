package com.example.stuffexchange.view

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.View
import androidx.activity.viewModels
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AppCompatActivity
import androidx.drawerlayout.widget.DrawerLayout
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import com.example.stuffexchange.R
import com.example.stuffexchange.viewmodel.MainActivityViewModel
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_main.drawerLayout
import kotlinx.android.synthetic.main.nav_header.*
import kotlinx.android.synthetic.main.nav_header.view.*
import kotlinx.android.synthetic.main.nav_header.view.nome

class MainActivity : AppCompatActivity() {

    private lateinit var appBarConfiguration: AppBarConfiguration
    private lateinit var navController: NavController
    private lateinit var viewModel: MainActivityViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)
        viewModel = ViewModelProvider(this).get(MainActivityViewModel::class.java)

        val toggle = ActionBarDrawerToggle(this, drawerLayout,toolbar, R.string.apri, R.string.chiudi)

        drawerLayout.addDrawerListener(toggle)
        toggle.syncState()

        navController = findNavController(R.id.navHostFragment)
        appBarConfiguration = AppBarConfiguration(setOf(
            R.id.splashFragment,
            R.id.loginFragment,
            R.id.registrazioneFragment,
            R.id.searchFragment,
            R.id.addFragment,
            R.id.chatFragment,
            R.id.oggettiCaricatiFragment,
            R.id.oggettiPrestatiFragment,
            R.id.oggettiPresiInPrestitoFragment,
            R.id.caricamentoCompletato,
            R.id.oggettiCaricatiDettagliFragment,
            R.id.oggettoAttivoDettagliFragment,
            R.id.chatFragment,
            R.id.messaggiFragment,
            R.id.mappaFragment,
            R.id.listaMappaFragment,
            R.id.mappaDettagliFragment
        ))

        setupActionBarWithNavController(navController, appBarConfiguration)
        navigationView.setupWithNavController(navController)
    }


    override fun onStop() {
        super.onStop()
        val header: View = navigationView.getHeaderView(0)
        viewModel.setNome(header.nome.text.toString())
        viewModel.setEmail(header.email.text.toString())
        viewModel.setVotoMedio(header.voto.text.toString())

    }

    override fun onResume() {
        super.onResume()
        val header: View = navigationView.getHeaderView(0)

        viewModel.getNome()?.let {
            header.nome.text = it
        }

        viewModel.getEmail()?.let {
            header.email.text = it
        }

        viewModel.getVotoMedio()?.let {
            header.voto.text = it
        }

    }


    fun setDrawerLocked(enabled: Boolean) {
        if (enabled) {
            drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED)
        } else {
            drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED)
        }
    }

    @SuppressLint("SetTextI18n")
    fun aggiornamentoDrawer(nome: String, cognome: String, mail:String, voto: Float, numeroVoti: Int){
        val header: View = navigationView.getHeaderView(0)
        val nomeCompleto = "$nome $cognome"
        header.nome.text = nomeCompleto
        header.email.text = mail
        header.voto.text = "$voto ($numeroVoti)"
    }


}
