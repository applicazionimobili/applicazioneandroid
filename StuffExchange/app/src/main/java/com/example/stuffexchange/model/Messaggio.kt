package com.example.stuffexchange.model

import java.util.*


data class Messaggio(
    var idUtente: String,
    var idMittente: String,
    var nomeMittente: String,
    var testo: String
) {

    constructor() : this("", "", "", "")
}