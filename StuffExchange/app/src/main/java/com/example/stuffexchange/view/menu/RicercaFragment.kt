package com.example.stuffexchange.view.menu

import android.content.Context
import android.content.Intent
import android.location.Criteria
import android.location.Geocoder
import android.location.LocationManager
import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.activity.addCallback
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.example.stuffexchange.R
import com.example.stuffexchange.model.Oggetto
import com.example.stuffexchange.util.CallbackPosizione
import com.example.stuffexchange.util.Caricamento
import com.example.stuffexchange.util.Connessione
import com.example.stuffexchange.util.Localizzazione
import com.example.stuffexchange.view.MainActivity
import com.example.stuffexchange.viewmodel.RicercaViewModel
import com.example.stuffexchange.viewmodel.ViewModelFactoryPosition
import com.karumi.dexter.Dexter
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionDeniedResponse
import com.karumi.dexter.listener.PermissionGrantedResponse
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.single.PermissionListener
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_ricerca.*
import java.util.*
import kotlin.collections.ArrayList


class RicercaFragment : Fragment() {

    private lateinit var viewModelFactory: ViewModelFactoryPosition
    private lateinit var viewModel: RicercaViewModel
    private lateinit var connessione: Connessione
    private lateinit var caricamento: Caricamento
    private lateinit var localizzazione : Localizzazione
    private lateinit var appCompatActivity: AppCompatActivity
    private var posizioneSpinner: Int = 0
    private lateinit var geocoder: Geocoder
    private lateinit var locationManager: LocationManager
    private lateinit var mainActivity: MainActivity

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        geocoder = Geocoder(requireActivity())
        locationManager = requireActivity().getSystemService(Context.LOCATION_SERVICE) as LocationManager
        viewModelFactory = ViewModelFactoryPosition(requireActivity().application, geocoder, locationManager)
        viewModel = ViewModelProvider(this, viewModelFactory).get(RicercaViewModel::class.java)
        caricamento = Caricamento(requireActivity())
        connessione = Connessione(requireContext())
        appCompatActivity = requireActivity() as AppCompatActivity
        mainActivity = requireActivity() as MainActivity
        return inflater.inflate(R.layout.fragment_ricerca, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mainActivity.toolbar.title = "Ricerca"
        localizzazione = Localizzazione(requireActivity())
        spinner_km.adapter = ArrayAdapter(
            requireActivity(),
            R.layout.support_simple_spinner_dropdown_item,
            viewModel.getListaSpinner()
        )

        spinner_km?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {
                posizioneSpinner = 0
            }

            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                posizioneSpinner = position
            }
        }

        aggiornaPosizione.setOnClickListener {
            val precisione = Criteria.ACCURACY_COARSE

            if(!connessione.controlloConnessione())
                Criteria.ACCURACY_FINE

            if(!localizzazione.geolocalizzazioneAttiva()) {
                localizzazione.dialogPosizione()
                return@setOnClickListener
            }

            Dexter.withActivity(activity)
                .withPermission(android.Manifest.permission.ACCESS_FINE_LOCATION)
                .withListener(object : PermissionListener {

                    override fun onPermissionGranted(response: PermissionGrantedResponse?) {
                        viewModel.richiestaAggiornamentoPosizione(object: CallbackPosizione{
                            override fun onNewLocationAviable(location: String) {
                                azioniAggiornamentoPosizione(location)
                            }
                        }, precisione)
                    }

                    override fun onPermissionRationaleShouldBeShown(
                        permission: PermissionRequest?,
                        token: PermissionToken?
                    ) {
                        token?.continuePermissionRequest()
                    }

                    override fun onPermissionDenied(response: PermissionDeniedResponse?) {
                        Toast.makeText(activity, "Devi accettare i permessi \nSe hai selezionato l'opzione dont't ask again, riabilitali dalle impostazione del telefono", Toast.LENGTH_SHORT)
                            .show()
                    }
                }).check()

        }

        cercaOggetto.setOnClickListener {
            cercaOggetti()
        }

        requireActivity().onBackPressedDispatcher.addCallback(this) {
            val intent = Intent(Intent.ACTION_MAIN)
            intent.addCategory(Intent.CATEGORY_HOME)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
            startActivity(intent)
        }
    }


    override fun onResume() {
        super.onResume()
        appCompatActivity.supportActionBar?.show()
        osservaCaricamento()
        osservaMessaggio()
        osservaCercaOggetto()

        viewModel.getPosizioneText()?.let {
            posizione.text = it
        }
    }

    override fun onStop() {
        super.onStop()
        caricamento.dismissDialog()
        viewModel.setPosizioneText(posizione.text.toString())
    }

    private fun azioniAggiornamentoPosizione(result: String?) {
        result?.let {
            if(it == "Posizione non trovata") {
                posizione.text = it
                check_posizione.visibility = View.GONE
                aggiornaPosizione.visibility = View.VISIBLE
                Toast.makeText(requireActivity(), "Impossibile trovare posizione: controllare connessione internet e geolocalizzazione. Puoi riprovare", Toast.LENGTH_SHORT).show()
            }
            else {
                posizione.text = it
                check_posizione.visibility = View.VISIBLE
                aggiornaPosizione.visibility = View.GONE
                cercaOggetto.visibility = View.VISIBLE
            }

            viewModel.setCaricamento(false)
        }
    }

    private fun osservaMessaggio() {
        viewModel.getMessggio().observe(viewLifecycleOwner, Observer {
            if (it != "noerror")
                Toast.makeText(requireActivity(), it, Toast.LENGTH_SHORT).show()
        })
    }

    private fun cercaOggetti() {
        if (!connessione.controlloConnessione()) {
            Toast.makeText(requireActivity(), "Connessione Internet assente", Toast.LENGTH_SHORT)
                .show()
            return
        }

        viewModel.cercaOggetti(
            nomeOggetto,
            posizione,
            viewModel.getListaSpinner()[posizioneSpinner]
        )?.observe(viewLifecycleOwner, Observer { eventWrapper ->
            val result = eventWrapper.getContentIfNotHandled()
            azioniRicercaOggetto(result)
        })
    }

    private fun osservaCercaOggetto() {
        viewModel.getListOggetti()?.observe(viewLifecycleOwner, Observer { eventWrapper ->
            val result = eventWrapper.getContentIfNotHandled()
            azioniRicercaOggetto(result)
        })
    }

    private fun azioniRicercaOggetto(result: ArrayList<Oggetto>?) {
        result?.let {
            if (it.size > 0) {
                val arrayOggetti = arrayOfNulls<Oggetto>(it.size)
                it.toArray(arrayOggetti)
                val action = RicercaFragmentDirections.actionSearchFragmentToMappaFragment(
                    arrayOggetti,
                    posizione.text.toString()
                )
                val navController = findNavController()

                if (navController.currentDestination?.id == R.id.searchFragment)
                    navController.navigate(action)
            } else {
                Toast.makeText(requireActivity(), "Nessun oggetto trovato", Toast.LENGTH_SHORT)
                    .show()
            }
            viewModel.setCaricamento(false)
        }
    }


    private fun osservaCaricamento() {
        viewModel.getCaricamento().observe(viewLifecycleOwner, Observer {
            if (it)
                caricamento.loadingAlertDialog()
            else
                caricamento.dismissDialog()
        })
    }

}

