package com.example.stuffexchange.model

import android.os.Parcel
import android.os.Parcelable

data class Oggetto(
    var idVenditore: String?,
    var nomeVenditore: String?,
    var id: String?,
    var imgUrl: String?,
    var nome: String?,
    var keywords: String?,
    var descrizione: String?,
    var posizione: String?,
    var startData: String?,
    var endData: String?,
    var stato: String?,
    var latitudine: Float,
    var longitudine: Float): Parcelable{

    constructor(parcel: Parcel) : this(
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readFloat(),
        parcel.readFloat()
    ) {
    }

    constructor() : this("","","", "", "", "", "", "", "", "", "", 0F, 0F)

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(idVenditore)
        parcel.writeString(id)
        parcel.writeString(imgUrl)
        parcel.writeString(nome)
        parcel.writeString(keywords)
        parcel.writeString(descrizione)
        parcel.writeString(posizione)
        parcel.writeString(startData)
        parcel.writeString(endData)
        parcel.writeString(stato)
        parcel.writeFloat(latitudine)
        parcel.writeFloat(longitudine)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Oggetto> {
        override fun createFromParcel(parcel: Parcel): Oggetto {
            return Oggetto(parcel)
        }

        override fun newArray(size: Int): Array<Oggetto?> {
            return arrayOfNulls(size)
        }
    }

}




