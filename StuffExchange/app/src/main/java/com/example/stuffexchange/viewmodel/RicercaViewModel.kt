package com.example.stuffexchange.viewmodel

import android.app.Application
import android.location.Address
import android.location.Geocoder
import android.location.LocationManager
import android.widget.EditText
import android.widget.TextView
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.stuffexchange.model.EventWrapper
import com.example.stuffexchange.model.Oggetto
import com.example.stuffexchange.repository.RicercaRepository
import com.example.stuffexchange.util.CallbackPosizione
import java.io.IOException
import java.lang.reflect.InvocationTargetException

class RicercaViewModel(application: Application, private val geocoder: Geocoder, locationManager: LocationManager) : ViewModel() {

    private val repository = RicercaRepository(application, locationManager, geocoder)

    private val listaSpinner = arrayOf(10,20,30,40,50)
    private var messaggio = MutableLiveData("noerror")
    private var caricamento = MutableLiveData(false)
    private lateinit var listaOggetti: LiveData<EventWrapper<ArrayList<Oggetto>>>
    private var posizione_text : String? = null

    fun cercaOggetti(nomeOggetto: EditText, posizione: TextView, distanza: Int): LiveData<EventWrapper<ArrayList<Oggetto>>>? {
        if(posizione.text.toString().isEmpty()) {
            messaggio.value = "Aggiorna la posizione prima di procedere"
            nomeOggetto.requestFocus()
            return null
        }

        if(nomeOggetto.text.toString().isEmpty()){
            messaggio.value = "Inserire nome oggetto"
            nomeOggetto.requestFocus()
            return null
        }

        caricamento.value = true
        var geoMatches: List<Address>? = null

        try {
            geoMatches = geocoder.getFromLocationName(posizione.text.toString(),1)
        }catch (e: IOException){
            e.printStackTrace()
        }catch (e: InvocationTargetException){
            e.printStackTrace()
        }

        return if(geoMatches != null){
            val latitudine = geoMatches[0].latitude.toFloat()
            val longitudine = geoMatches[0].longitude.toFloat()
            listaOggetti = repository.cercaOggetti(nomeOggetto.text.toString(), latitudine, longitudine, distanza)
            listaOggetti
        } else {
            messaggio.value = "Errore geocodeer"
            null
        }
    }

    fun richiestaAggiornamentoPosizione(callback: CallbackPosizione, precisione: Int){
        caricamento.value = true
        repository.richiestaAggiornamentoPosizione(callback, precisione)
    }

    fun setCaricamento(caricamento: Boolean) {
        this.caricamento.value = caricamento
    }

    fun setPosizioneText(posizione_text: String) {
        this.posizione_text = posizione_text
    }

    fun getListOggetti() : LiveData<EventWrapper<ArrayList<Oggetto>>>? {
        return if(this::listaOggetti.isInitialized)
            listaOggetti
        else
            null
    }

    fun getListaSpinner(): Array<Int> = listaSpinner
    fun getCaricamento() : LiveData<Boolean> = caricamento
    fun getMessggio(): LiveData<String> = messaggio
    fun getPosizioneText() : String? = posizione_text
}