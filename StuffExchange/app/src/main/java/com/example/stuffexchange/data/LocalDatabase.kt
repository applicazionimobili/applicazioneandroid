package com.example.stuffexchange.data

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.example.stuffexchange.model.Utente


@Database(entities = arrayOf(Utente::class), version = 1)
abstract class LocalDatabase() : RoomDatabase() {
    abstract fun getUtenteDAO(): UtenteDAO

    companion object{
        private var INSTANCE: LocalDatabase? = null
        fun getInstance(context: Context): LocalDatabase {
            if(INSTANCE == null){
                INSTANCE = Room.databaseBuilder(context, LocalDatabase::class.java, "Local Database").build()
            }
            return INSTANCE as LocalDatabase
        }
    }
}