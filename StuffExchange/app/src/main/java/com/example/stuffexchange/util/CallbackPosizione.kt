package com.example.stuffexchange.util

interface CallbackPosizione {

    fun onNewLocationAviable(location: String)
}