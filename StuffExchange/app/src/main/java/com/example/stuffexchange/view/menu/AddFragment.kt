package com.example.stuffexchange.view.menu

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.app.AlertDialog
import android.app.DatePickerDialog
import android.content.ContentValues
import android.content.Context
import android.content.Intent
import android.location.Criteria
import android.location.Geocoder
import android.location.LocationManager
import android.os.Bundle
import android.os.Handler
import android.provider.MediaStore
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.activity.addCallback
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.example.stuffexchange.R
import com.example.stuffexchange.util.CallbackPosizione
import com.example.stuffexchange.util.Caricamento
import com.example.stuffexchange.util.Connessione
import com.example.stuffexchange.util.Localizzazione
import com.example.stuffexchange.view.MainActivity
import com.example.stuffexchange.viewmodel.AddViewModel
import com.example.stuffexchange.viewmodel.ViewModelFactoryPosition
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionDeniedResponse
import com.karumi.dexter.listener.PermissionGrantedResponse
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import com.karumi.dexter.listener.single.PermissionListener
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_add.*


class AddFragment : Fragment() {

    private lateinit var viewModelFactory: ViewModelFactoryPosition
    private lateinit var viewModel: AddViewModel
    private lateinit var caricamento: Caricamento
    private lateinit var connessione: Connessione
    private lateinit var localizzazione: Localizzazione
    private lateinit var geocoder: Geocoder
    private lateinit var locationManager: LocationManager
    private lateinit var mainActivity: MainActivity

    private val CAMERA_CODE = 1001
    private val GALLERY_CODE = 1002

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        geocoder = Geocoder(requireActivity())
        locationManager = requireActivity().getSystemService(Context.LOCATION_SERVICE) as LocationManager
        viewModelFactory = ViewModelFactoryPosition(requireActivity().application, geocoder, locationManager)
        viewModel = ViewModelProvider(this, viewModelFactory).get(AddViewModel::class.java)
        caricamento = Caricamento(requireActivity())
        connessione = Connessione(requireActivity())
        mainActivity = requireActivity() as MainActivity

        return inflater.inflate(R.layout.fragment_add, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        mainActivity.toolbar.title = "Aggiungi oggetto"
        localizzazione = Localizzazione(requireActivity())
        observeKeyword()
        observeMessage()

        aggiornaPosizione.setOnClickListener {
            val precisione = Criteria.ACCURACY_COARSE

            if(!connessione.controlloConnessione())
                Criteria.ACCURACY_FINE

            if(!localizzazione.geolocalizzazioneAttiva()) {
                localizzazione.dialogPosizione()
                return@setOnClickListener
            }


            Dexter.withActivity(activity)
                .withPermission(Manifest.permission.ACCESS_FINE_LOCATION)
                .withListener(object : PermissionListener {
                    @SuppressLint("SourceLockedOrientationActivity")
                    override fun onPermissionGranted(response: PermissionGrantedResponse?) {
                        viewModel.richiestaAggiornamentoPosizione(object: CallbackPosizione {
                            override fun onNewLocationAviable(location: String) {
                                azioniAggiornamentoPosizione(location)
                            }
                        }, precisione)
                    }

                    override fun onPermissionRationaleShouldBeShown(
                        permission: PermissionRequest?,
                        token: PermissionToken?
                    ) {
                        token?.continuePermissionRequest()
                    }

                    override fun onPermissionDenied(response: PermissionDeniedResponse?) {
                        Toast.makeText(activity, "Devi accettare i permessi \nSe hai selezionato l'opzione dont't ask again, riabilitali dalle impostazione del telefono", Toast.LENGTH_SHORT)
                            .show()
                    }
                }).check()
        }

        floating_btn.setOnClickListener {

            Dexter.withActivity(activity)
                .withPermissions(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .withListener(object: MultiplePermissionsListener {

                    override fun onPermissionsChecked(report: MultiplePermissionsReport?) {
                        report?.let {
                            if(report.areAllPermissionsGranted()){
                                selezionaImmagine()
                            }else
                                Toast.makeText(requireActivity(), "Tutti i permessi devono essere accettati", Toast.LENGTH_SHORT).show()
                        }
                    }

                    override fun onPermissionRationaleShouldBeShown(permissions: MutableList<PermissionRequest>?, token: PermissionToken?) {
                       token?.continuePermissionRequest()
                    }
                }).check()
        }

        caricaOggetto.setOnClickListener {
            caricaOggetto()
        }

        startDate.setOnClickListener {
            selezionaData(startDate)
        }

        endDate.setOnClickListener {
            selezionaData(endDate)
        }

        aggiungiKeywords.setOnClickListener {
            aggiungiKeywords()
        }

        eliminaKeywords.setOnClickListener {
            rimuoviKeywords()
        }

        eliminaNome.setOnClickListener {
            rimuoviNome()
        }

        eliminaDescrizione.setOnClickListener {
            rimuoviDescrizione()
        }

        requireActivity().onBackPressedDispatcher.addCallback(this) {
            val navController = findNavController()

            if(navController.currentDestination?.id == R.id.addFragment)
                navController.navigate(R.id.action_addFragment_to_searchFragment)
        }
    }

    override fun onResume() {
        super.onResume()
        ooservaCaricamento()
        osservaCaricamentoOggetto()

        viewModel.getImageUri()?.let {
            Picasso.get().load(it).fit().into(imageView)
        }

        viewModel.getStartDate()?.let {
            startDate.text = it
        }

        viewModel.getEndDate()?.let {
            endDate.text = it
        }
    }


    override fun onStop() {
        super.onStop()
        caricamento.dismissDialog()
        viewModel.setStartDate(startDate.text.toString())
        viewModel.setEndDate(endDate.text.toString())
    }

    private fun ooservaCaricamento() {
        viewModel.getCaricamento().observe(viewLifecycleOwner, Observer {
            if(it)
                caricamento.loadingAlertDialog()
            else
                caricamento.dismissDialog()
        })
    }

    private fun azioniAggiornamentoPosizione(result: String?) {
        result?.let {
            if(it == "Posizione non trovata") {
                posizione.text = it
                check_posizione.visibility = View.GONE
                aggiornaPosizione.visibility = View.VISIBLE
                caricaOggetto.visibility = View.GONE
                Toast.makeText(requireActivity(), "Impossibile trovare posizione: controllare connessione internet e geolocalizzazione. Puoi riprovare", Toast.LENGTH_SHORT).show()
            }
            else {
                posizione.text = it
                check_posizione.visibility = View.VISIBLE
                aggiornaPosizione.visibility = View.GONE
                caricaOggetto.visibility = View.VISIBLE
            }

            viewModel.setCaricamento(false)
        }
    }

    fun observeKeyword(){
        viewModel.getListaKeyoword().observe(viewLifecycleOwner, Observer {
            listaKeywords.text = it

            if(it == " " || it.isEmpty()) {
                listaKeywords.visibility = View.GONE
                eliminaKeywords.visibility = View.GONE
                text_keywords.visibility = View.GONE
            }
            else{
                listaKeywords.visibility = View.VISIBLE
                eliminaKeywords.visibility = View.VISIBLE
                text_keywords.visibility = View.VISIBLE
            }

            keywords.text.clear()
        })
    }

    fun observeMessage(){
        viewModel.getMessage().observe(viewLifecycleOwner, Observer {
            if(it != "noerror")
                Toast.makeText(requireActivity(), it,  Toast.LENGTH_SHORT).show()
        })
    }

    @SuppressLint("SourceLockedOrientationActivity")
    fun caricaOggetto(){
        if(!connessione.controlloConnessione()){
            Toast.makeText(requireActivity(), "Connessione assente", Toast.LENGTH_SHORT).show()
            return
        }

        if(!viewModel.controlloCampi(nomeOggetto, descrizioneOggetto, posizione, startDate, endDate, listaKeywords)){
            return
        }

        viewModel.caricaOggetto(nomeOggetto, descrizioneOggetto, posizione, startDate, endDate)
            ?.observe(viewLifecycleOwner, Observer {eventWrapper->
                val result = eventWrapper.getContentIfNotHandled()
                azioniCaricaOggetto(result)
            })
    }

    private fun osservaCaricamentoOggetto() {
        viewModel.getCaricaOggettoSuccess()?.observe(viewLifecycleOwner, Observer {eventWrapper->
            val result = eventWrapper.getContentIfNotHandled()
            azioniCaricaOggetto(result)
        })
    }

    private fun azioniCaricaOggetto(result: Boolean?){
        result?.let {
            if(it) {
                Toast.makeText(requireActivity(), "Oggetto caricato", Toast.LENGTH_SHORT).show()
                viewModel.setImageUri(null)
                viewModel.setCaricamento(false)
                val action = AddFragmentDirections.actionAddFragmentToCaricamentoCompletato(0)
                val navController = findNavController()

                if(navController.currentDestination?.id == R.id.addFragment)
                    navController.navigate(action)
            } else {
                viewModel.setCaricamento(false)
                Toast.makeText(requireActivity(), "Oggetto non caricato", Toast.LENGTH_SHORT).show()
            }
        }
    }

    fun selezionaData(dataText: TextView){
        viewModel.selezionaData(dataText.text.toString())

        DatePickerDialog(requireActivity(), DatePickerDialog.OnDateSetListener{_, anno, mese, giorno ->
            val aux = "$giorno/${mese+1}/$anno"
            dataText.text = aux
            dataText.error = null
        }, viewModel.getAnno(), viewModel.getMese()-1, viewModel.getGiorno()).show()
    }



    private fun selezionaImmagine() {
        val opzioni = arrayOf<CharSequence>("Scatta una foto","Scegli dalla galleria", "Annulla")
        val builder = AlertDialog.Builder(requireActivity())
        builder.setTitle("Scegli la sorgente della foto")
        builder.setItems(opzioni){ dialog, item ->

            when{
                opzioni[item] == "Scatta una foto"->{
                    apriCamera()
                }

                opzioni[item] == "Scegli dalla galleria"->{
                    apriGalleria()
                }

                opzioni[item] == "Annulla" ->{
                    dialog.dismiss()
                }
            }
        }

        builder.show()
    }

    private fun apriGalleria() {
        val intent = Intent().apply {
            action = Intent.ACTION_PICK
            type = "image/*"
        }

        startActivityForResult(intent, GALLERY_CODE)
    }

    private fun apriCamera() {
        val valori = ContentValues()
        valori.put(MediaStore.Images.Media.TITLE, "Nuova immagine")
        viewModel.setImageUri(requireActivity().contentResolver.insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, valori))
        val cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, viewModel.getImageUri())
        startActivityForResult(cameraIntent, CAMERA_CODE)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if(resultCode == Activity.RESULT_CANCELED){
            Toast.makeText(requireActivity(), "Operazione cancellata", Toast.LENGTH_SHORT).show()
            return
        }

        when(requestCode){

            CAMERA_CODE->{
                Picasso.get().load(viewModel.getImageUri()).fit().placeholder(R.drawable.caricamento).into(imageView)
            }

            GALLERY_CODE->{
                data?.data?.let {
                    viewModel.setImageUri(it)
                    Picasso.get().load(it).fit().placeholder(R.drawable.caricamento).into(imageView)
                }
            }

        }
    }

    fun rimuoviNome(){
        viewModel.rimuoviNomeOggetto(nomeOggetto)
    }

    fun rimuoviDescrizione(){
        viewModel.rimuoviDescrizione(descrizioneOggetto)
    }

    fun aggiungiKeywords(){
        viewModel.aggiungiKeywords(keywords)
    }

    fun rimuoviKeywords(){
        viewModel.rimuoviKeywords()
    }
}
