package com.example.stuffexchange.model

data class NotificaOggetto(
    var caricato: Boolean,
    var prestato: Boolean,
    var inPrestito: Boolean) {
}