package com.example.stuffexchange.view.list

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.example.stuffexchange.R
import com.example.stuffexchange.util.Caricamento
import com.example.stuffexchange.util.Connessione
import com.example.stuffexchange.view.MainActivity
import com.example.stuffexchange.viewmodel.OggettiViewModel
import com.example.stuffexchange.viewmodel.ViewModelFactory
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_oggetti_caricati_dettagli.*
import kotlinx.android.synthetic.main.fragment_oggetti_caricati_dettagli.descrizione
import kotlinx.android.synthetic.main.fragment_oggetti_caricati_dettagli.endDate
import kotlinx.android.synthetic.main.fragment_oggetti_caricati_dettagli.imageView
import kotlinx.android.synthetic.main.fragment_oggetti_caricati_dettagli.keywords
import kotlinx.android.synthetic.main.fragment_oggetti_caricati_dettagli.nome
import kotlinx.android.synthetic.main.fragment_oggetti_caricati_dettagli.posizione
import kotlinx.android.synthetic.main.fragment_oggetti_caricati_dettagli.startDate



class OggettiCaricatiDettagliFragment : Fragment() {

    private lateinit var viewModelFactory: ViewModelFactory
    private lateinit var viewModel: OggettiViewModel
    private lateinit var caricamento: Caricamento
    private lateinit var connessione: Connessione
    private lateinit var mainActivity: MainActivity

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        viewModelFactory = ViewModelFactory(requireActivity().application)
        viewModel = ViewModelProvider(this, viewModelFactory).get(OggettiViewModel::class.java)
        caricamento = Caricamento(requireActivity())
        connessione = Connessione(requireActivity())
        mainActivity = requireActivity() as MainActivity

        return inflater.inflate(R.layout.fragment_oggetti_caricati_dettagli, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mainActivity.toolbar.title = "Dettaglio oggetto"

        elimina.setOnClickListener {
            eliminaOggetto()
        }

    }

    override fun onResume() {
        super.onResume()
        aggiornamentoDettaglio()
        osservaCaricamento()
        osservaEliminaOggetto()
    }

    override fun onStop() {
        super.onStop()
        caricamento.dismissDialog()
    }

    private fun osservaCaricamento() {
        viewModel.getCaricamento().observe(viewLifecycleOwner, Observer {
            if(it)
                caricamento.loadingAlertDialog()
            else
                caricamento.dismissDialog()
        })
    }

    fun aggiornamentoDettaglio(){
        arguments?.let {bundle->
            viewModel.aggiornamentoDettaglio(OggettiCaricatiDettagliFragmentArgs.fromBundle(bundle).oggetto).observe(viewLifecycleOwner, Observer {
                it?.let {oggetto->
                    nome.text = oggetto.nome
                    posizione.text = oggetto.posizione
                    descrizione.text = oggetto.descrizione
                    keywords.text = oggetto.keywords
                    startDate.text = oggetto.startData
                    endDate.text = oggetto.endData
                    Picasso.get().load(oggetto.imgUrl).fit().placeholder(R.drawable.caricamento).into(imageView)

                    if(oggetto.stato == "expired")
                        elimina.visibility = View.VISIBLE
                    else
                       elimina.visibility = View.GONE
                }
            })
        }
    }

    private fun eliminaOggetto() {
        if (!connessione.controlloConnessione()) {
            Toast.makeText(activity, "Connessione internet assente", Toast.LENGTH_SHORT).show()
            return
        }

        viewModel.eliminaOggetto()?.observe(viewLifecycleOwner, Observer { eventWrapper ->
            val result = eventWrapper.getContentIfNotHandled()
            azioniEliminaOggetto(result)
        })
    }

    private fun osservaEliminaOggetto() {
        viewModel.getSuccessElimina()?.observe(viewLifecycleOwner, Observer { eventWrapper ->
            val result = eventWrapper.getContentIfNotHandled()
            azioniEliminaOggetto(result)
        })
    }

    private fun azioniEliminaOggetto(result: Boolean?) {
        result?.let {
            if(it){
                Toast.makeText(requireActivity(), "Oggetto eliminato dalla lista", Toast.LENGTH_SHORT).show()
                val navController = findNavController()

                if(navController.currentDestination?.id == R.id.oggettiCaricatiDettagliFragment)
                    navController.navigate(R.id.action_oggettiCaricatiDettagliFragment_to_oggettiCaricatiFragment)
            }
            else
                Toast.makeText(requireActivity(), "Oggetto non eliminato dalla lista", Toast.LENGTH_SHORT).show()

            viewModel.setCaricamento(false)
        }
    }

}
