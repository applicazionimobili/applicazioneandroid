package com.example.stuffexchange.model

import android.os.Parcel
import android.os.Parcelable
import androidx.room.*

@Entity(tableName = "Utente")
class Utente(
    @PrimaryKey
    var primaryKey: Int,

    @ColumnInfo(name = "Id")
    var id: String,

    @ColumnInfo(name = "Nome")
    var nome: String,

    @ColumnInfo(name = "Cognome")
    var cognome: String,

    @ColumnInfo(name = "Mail")
    var mail: String,

    @ColumnInfo(name = "Numero operazioni")
    var numeroOperazioni: Int,

    @ColumnInfo(name = "Numero voti")
    var numeroVoti: Int,

    @ColumnInfo(name = "Voto medio")
    var votoMedio: Float): Parcelable{

    constructor(parcel: Parcel) : this(
        parcel.readInt(),
        parcel.readString()!!,
        parcel.readString()!!,
        parcel.readString()!!,
        parcel.readString()!!,
        parcel.readInt(),
        parcel.readInt(),
        parcel.readFloat()
    ) {
    }

    constructor(): this(1, "", "", "", "", 0,0, 0.toFloat())

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(primaryKey)
        parcel.writeString(id)
        parcel.writeString(nome)
        parcel.writeString(cognome)
        parcel.writeString(mail)
        parcel.writeInt(numeroOperazioni)
        parcel.writeInt(numeroVoti)
        parcel.writeFloat(votoMedio)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Utente> {
        override fun createFromParcel(parcel: Parcel): Utente {
            return Utente(parcel)
        }

        override fun newArray(size: Int): Array<Utente?> {
            return arrayOfNulls(size)
        }
    }
}



