package com.example.stuffexchange.view.menu

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.SearchView
import android.widget.Toast
import androidx.activity.addCallback
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.example.stuffexchange.R
import com.example.stuffexchange.model.UtenteChat
import com.example.stuffexchange.util.Caricamento
import com.example.stuffexchange.util.Connessione
import com.example.stuffexchange.view.MainActivity
import com.example.stuffexchange.view.adapter.ChatAdapter
import com.example.stuffexchange.viewmodel.ChatViewModel
import com.example.stuffexchange.viewmodel.ViewModelFactory
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_chat.*


@Suppress("DEPRECATION")
class ChatFragment : Fragment(), SwipeRefreshLayout.OnRefreshListener, SearchView.OnQueryTextListener {

    private lateinit var viewModelFactory: ViewModelFactory
    private lateinit var viewModel: ChatViewModel
    private lateinit var connessione: Connessione
    private lateinit var caricamento: Caricamento
    private var chatAdapter = ChatAdapter(arrayListOf())
    private lateinit var mainActivity: MainActivity



    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        viewModelFactory = ViewModelFactory(requireActivity().application)
        viewModel = ViewModelProvider(this, viewModelFactory).get(ChatViewModel::class.java)
        connessione = Connessione(requireActivity())
        caricamento = Caricamento(requireActivity())
        mainActivity = requireActivity() as MainActivity

        return inflater.inflate(R.layout.fragment_chat, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mainActivity.toolbar.title = "Chat"

        listaUtenti.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = chatAdapter
        }

        swipe.setColorSchemeColors(requireActivity().resources.getColor(R.color.colorAccent))
        swipe.setOnRefreshListener(this)

        requireActivity().onBackPressedDispatcher.addCallback(this) {
            val navController = findNavController()

            if(navController.currentDestination?.id == R.id.chatFragment)
                navController.navigate(R.id.action_chatFragment_to_searchFragment)
        }

        searchView.setOnQueryTextListener(this)
    }

    private fun filtraLista(query: String?) {
        if(!connessione.controlloConnessione()){
            Toast.makeText(requireActivity(), "Connessione Internet assente", Toast.LENGTH_SHORT).show()
            return
        }

        viewModel.aggiornaListaChat(query).observe(viewLifecycleOwner, Observer {
            azioniListaUtenti(it)
        })
    }

    override fun onResume() {
        super.onResume()
        osservaCaricamento()

        if(viewModel.getCaricamento().value!!)
            osservaListaUtenti()
        else
            aggiornaListaUtenti()
    }

    override fun onStop() {
        super.onStop()
        caricamento.dismissDialog()
    }

    private fun aggiornaListaUtenti() {
        if(!connessione.controlloConnessione()){
            Toast.makeText(requireActivity(), "Connessione Internet assente", Toast.LENGTH_SHORT).show()
            textEmpty.visibility = View.VISIBLE
            return
        }

        viewModel.aggiornaListaChat(null).observe(viewLifecycleOwner, Observer {
            azioniListaUtenti(it)
        })
    }

    private fun osservaListaUtenti() {
        viewModel.getListaChat()?.observe(viewLifecycleOwner, Observer { it ->
            azioniListaUtenti(it)
        })
    }

    fun azioniListaUtenti(result: ArrayList<UtenteChat>?) {
        result?.let {
            println("SIZE IT " + it.size)
            if(it.size == 0) {
                textEmpty.visibility = View.VISIBLE
                chatAdapter.aggiornaLista(it)
            }
            else {
                listaUtenti.visibility = View.VISIBLE
                textEmpty.visibility = View.GONE
                chatAdapter.aggiornaLista(it)
            }

            viewModel.setCaricamento(false)
        }
    }

    private fun osservaCaricamento() {
        viewModel.getCaricamento().observe(viewLifecycleOwner, Observer {
            if(it)
                caricamento.loadingAlertDialog()
            else {
                caricamento.dismissDialog()
                swipe.isRefreshing = false
            }
        })
    }

    override fun onRefresh() {
        if (!connessione.controlloConnessione()) {
            Toast.makeText(activity, "Connessione Internet assente", Toast.LENGTH_SHORT).show()
            textEmpty.visibility = View.VISIBLE
            swipe.isRefreshing = false
            listaUtenti.visibility = View.GONE
            return
        }else {
            aggiornaListaUtenti()
            textEmpty.visibility = View.GONE
        }
    }

    override fun onQueryTextSubmit(query: String?): Boolean {
        filtraLista(query)

        return true
    }

    override fun onQueryTextChange(newText: String?): Boolean {
       if(newText == null || newText.isEmpty())
         filtraLista(newText)

        return true
    }
}
