package com.example.stuffexchange.util

import android.content.Context
import android.net.ConnectivityManager

class Connessione(private val context: Context) {
    fun controlloConnessione(): Boolean {
        val cm: ConnectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val networkInfo = cm.activeNetworkInfo

        return if (networkInfo != null && networkInfo.isConnectedOrConnecting) {
            val wifi: android.net.NetworkInfo = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI)
            val mobile: android.net.NetworkInfo = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE)
            (mobile.isConnectedOrConnecting || (wifi.isConnectedOrConnecting))
        }
        else
            false
    }
}