package com.example.stuffexchange.viewmodel

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.stuffexchange.model.EventWrapper
import com.example.stuffexchange.model.Oggetto
import com.example.stuffexchange.model.OggettoAttivo
import com.example.stuffexchange.repository.OggettiRepository

class OggettiViewModel(application: Application): ViewModel() {
    private val repository: OggettiRepository = OggettiRepository(application)

    private lateinit var listaOggettiCaricati : LiveData<ArrayList<Oggetto>>
    private lateinit var listaOggettiPresiInPrestito : LiveData<ArrayList<OggettoAttivo>>
    private lateinit var listaOggettiPrestati: LiveData<ArrayList<OggettoAttivo>>
    private lateinit var eliminaSuccess: LiveData<EventWrapper<Boolean>>
    private lateinit var successRecensione: LiveData<EventWrapper<Boolean>>

    private var oggetto = MutableLiveData<Oggetto>()
    private var oggettoAttivo = MutableLiveData<OggettoAttivo>()
    private var caricamento = MutableLiveData(false)

    private var rating = 0F

    fun scaricaListaOggettiCaricati(): LiveData<ArrayList<Oggetto>> {
        listaOggettiCaricati = repository.scaricaListaOgettiCaricati()
        return listaOggettiCaricati
    }

    fun aggiornaListaInPrestito(): LiveData<ArrayList<OggettoAttivo>> {
        listaOggettiPresiInPrestito = repository.aggiornaListaInPrestito()
        return listaOggettiPresiInPrestito
    }

    fun aggiornaListaPrestati(): LiveData<ArrayList<OggettoAttivo>> {
        listaOggettiPrestati = repository.aggiornaListaPrestati()
        return listaOggettiPrestati
    }

    fun aggiornamentoDettaglio(oggetto: Oggetto): LiveData<Oggetto> {
        oggetto.let {
            this.oggetto.value = it
            return this.oggetto
        }
    }


    fun eliminaOggetto(): LiveData<EventWrapper<Boolean>>? {
        caricamento.value = true
        oggetto.value?.let {
            eliminaSuccess = repository.eliminaOggetto(oggetto.value!!.id!!)
            return eliminaSuccess
        }

        return null
    }

    fun aggiornamentoDettaglioOggettoAttivo(oggetto: OggettoAttivo): LiveData<OggettoAttivo> {
        oggetto.let {
            oggettoAttivo.value = oggetto
            return oggettoAttivo
        }
    }

    fun setRating(rating: Float){
        this.rating = rating
    }

    fun recensioneUtente(codiceDestinazione: Int): LiveData<EventWrapper<Boolean>>? {
        oggettoAttivo.value?.let {
            caricamento.value = true
            successRecensione = repository.recensioneUtente(rating, oggettoAttivo.value as OggettoAttivo, codiceDestinazione)
            return successRecensione
        }

        return null
    }

    fun getCaricamento(): LiveData<Boolean> = caricamento

    fun setCaricamento(caricamento: Boolean){
        this.caricamento.value = caricamento
    }

    fun getSuccessElimina() : LiveData<EventWrapper<Boolean>>? {
        return if(this::eliminaSuccess.isInitialized)
            eliminaSuccess
        else
            return null
    }

    fun getSuccessRecensione(): LiveData<EventWrapper<Boolean>>?{
        return if(this::successRecensione.isInitialized)
            successRecensione
        else
            null

    }

    fun getOggettoAttivo(): LiveData<OggettoAttivo>{
        return oggettoAttivo
    }
}