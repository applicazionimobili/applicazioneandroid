package com.example.stuffexchange.view.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.stuffexchange.R
import com.example.stuffexchange.model.Messaggio
import kotlinx.android.synthetic.main.messaggio_inviato.view.*
import kotlinx.android.synthetic.main.messaggio_inviato.view.testoMessaggio
import kotlinx.android.synthetic.main.messaggio_ricevuto.view.*

class MessaggiAdapter(private val messages: ArrayList<Messaggio>) : RecyclerView.Adapter<MessaggiAdapter.MessaggiAdapterViewHolder>() {

    inner class MessaggiAdapterViewHolder(var view: View) : RecyclerView.ViewHolder(view)


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MessaggiAdapterViewHolder {
        val inflater = LayoutInflater.from(parent.context)

        return if (viewType == 1) {
            val view = inflater.inflate(R.layout.messaggio_inviato, parent, false)
            MessaggiAdapterViewHolder(view)
        } else {
            val view = inflater.inflate(R.layout.messaggio_ricevuto, parent, false)
            MessaggiAdapterViewHolder(view)
        }
    }

    override fun getItemCount(): Int = messages.size


    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: MessaggiAdapterViewHolder, position: Int) {
        holder.view.testoMessaggio.text = messages[position].testo

        if (messages[position].idUtente == messages[position].idMittente)
            holder.view.testoMessaggioInviato.text = "me"
        else
            holder.view.testoMessaggioRicevuto.text = "other"
    }



    override fun getItemViewType(position: Int): Int {
        return if (messages[position].idUtente == messages[position].idMittente)
            1
        else
            2
    }


    fun updateObjectMessages(newMessages: ArrayList<Messaggio>) {
        messages.clear()
        messages.addAll(newMessages)
        notifyDataSetChanged()
    }
}