package com.example.stuffexchange.view.menu

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.activity.addCallback
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController

import com.example.stuffexchange.R
import com.example.stuffexchange.util.Connessione
import com.example.stuffexchange.view.MainActivity
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_caricamento_completato.*

/**
 * A simple [Fragment] subclass.
 */
class CaricamentoCompletato : Fragment() {

    private lateinit var connessione : Connessione
    private val CODICE_PRESTATO = 1
    private val CODICE_IN_PRESTITO = 2
    private val CODICE_CONFERMA_PRENOTAZIONE = 3
    private lateinit var navController : NavController
    private lateinit var mainActivity: MainActivity

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        connessione = Connessione(requireActivity())
        navController = findNavController()
        mainActivity = requireActivity() as MainActivity
        return inflater.inflate(R.layout.fragment_caricamento_completato, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mainActivity.toolbar.title = "Caricamento completato"

        arguments?.let {
            val codice = CaricamentoCompletatoArgs.fromBundle(it).codiceDestinazione

            when(codice){
                CODICE_PRESTATO->{
                    back_btn.setOnClickListener {
                        if(!connessione.controlloConnessione()){
                            Toast.makeText(requireActivity(), "Connessione assente", Toast.LENGTH_SHORT).show()
                            return@setOnClickListener
                        }

                        if (navController.currentDestination?.id == R.id.caricamentoCompletato)
                            navController.navigate(R.id.action_caricamentoCompletato_to_oggettiPrestatiFragment)
                    }

                    requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner){
                        if (navController.currentDestination?.id == R.id.caricamentoCompletato)
                            navController.navigate(R.id.action_caricamentoCompletato_to_oggettiPrestatiFragment)
                    }
                }

                CODICE_IN_PRESTITO->{
                    back_btn.setOnClickListener {
                        if(!connessione.controlloConnessione()){
                            Toast.makeText(requireActivity(), "Connessione assente", Toast.LENGTH_SHORT).show()
                            return@setOnClickListener
                        }

                        if (navController.currentDestination?.id == R.id.caricamentoCompletato)
                            navController.navigate(R.id.action_caricamentoCompletato_to_oggettiPresiInPrestitoFragment)
                    }

                    requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner){
                        if (navController.currentDestination?.id == R.id.caricamentoCompletato)
                            navController.navigate(R.id.action_caricamentoCompletato_to_oggettiPresiInPrestitoFragment)
                    }
                }

                CODICE_CONFERMA_PRENOTAZIONE-> {
                    back_btn.setOnClickListener {
                        if (!connessione.controlloConnessione()) {
                            Toast.makeText(requireActivity(), "Connessione assente", Toast.LENGTH_SHORT).show()
                            return@setOnClickListener
                        }

                        if (navController.currentDestination?.id == R.id.caricamentoCompletato)
                            navController.navigate(R.id.action_caricamentoCompletato_to_searchFragment)
                    }

                    requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner){
                        if (navController.currentDestination?.id == R.id.caricamentoCompletato)
                            navController.navigate(R.id.action_caricamentoCompletato_to_searchFragment)
                    }
                }

                else-> {
                    back_btn.setOnClickListener {
                        if(!connessione.controlloConnessione()){
                            Toast.makeText(requireActivity(), "Connessione assente", Toast.LENGTH_SHORT).show()
                            return@setOnClickListener
                        }

                        if (navController.currentDestination?.id == R.id.caricamentoCompletato)
                            navController.navigate(R.id.action_caricamentoCompletato_to_searchFragment)
                    }

                    requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner){
                        if (navController.currentDestination?.id == R.id.caricamentoCompletato)
                            navController.navigate(R.id.action_caricamentoCompletato_to_searchFragment)
                    }
                }
            }
        }
    }

}
