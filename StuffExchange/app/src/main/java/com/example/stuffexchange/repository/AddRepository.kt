package com.example.stuffexchange.repository

import android.Manifest
import android.annotation.SuppressLint
import android.app.Application
import android.content.pm.PackageManager
import android.location.*
import android.net.Uri
import android.os.AsyncTask
import android.os.Bundle
import androidx.core.app.ActivityCompat
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.stuffexchange.data.FirebaseDatabase
import com.example.stuffexchange.data.LocalDatabase
import com.example.stuffexchange.model.EventWrapper
import com.example.stuffexchange.model.Oggetto
import com.example.stuffexchange.util.CallbackPosizione
import com.google.android.gms.tasks.OnFailureListener
import com.google.android.gms.tasks.OnSuccessListener
import java.io.IOException
import java.lang.reflect.InvocationTargetException

class AddRepository(private val application: Application, private val geocoder: Geocoder, private val locationManager: LocationManager) {

    private val localeDatabase: LocalDatabase = LocalDatabase.getInstance(application)
    private val database: FirebaseDatabase = FirebaseDatabase.instance

    fun caricaOggetto(oggetto: Oggetto, imageUri: Uri?): LiveData<EventWrapper<Boolean>>{
        val success = MutableLiveData<EventWrapper<Boolean>>()

        val caricaOggettoSuccessListener = OnSuccessListener<Void>{
            UpdateNumeroOperationiTask(localeDatabase,success).execute()
        }

        val caricaOggettoFailureListener = OnFailureListener{
            success.value = EventWrapper(false)
        }

        database.caricaOggetto(oggetto, imageUri, caricaOggettoSuccessListener, caricaOggettoFailureListener)
        return success
    }

    /*Aggiorna il numero delle operazioni nel database locale*/
    class UpdateNumeroOperationiTask(private val db: LocalDatabase, private val success:MutableLiveData<EventWrapper<Boolean>>): AsyncTask<Int, Void, Void>(){
        override fun doInBackground(vararg params: Int?): Void? {
            db.getUtenteDAO().updateNumeroOperazioni()
            return null
        }

        override fun onPostExecute(result: Void?) {
            super.onPostExecute(result)
            success.value = EventWrapper(true)
        }
    }

    fun richiestaAggiornamentoPosizione(callback: CallbackPosizione, precisione: Int) {
        val criteria = Criteria()
        criteria.accuracy = precisione

        if (ActivityCompat.checkSelfPermission(application, android.Manifest.permission.ACCESS_FINE_LOCATION)
            != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(application,
                Manifest.permission.ACCESS_COARSE_LOCATION)
            != PackageManager.PERMISSION_GRANTED) {}

        locationManager.requestSingleUpdate(criteria,
            object : LocationListener {
                override fun onLocationChanged(location: Location?) {

                    location?.let {
                        var geoMatches : List<Address>? = null

                        try {
                            geoMatches = geocoder.getFromLocation(location.latitude, location.longitude, 1)
                        }catch(e: IOException){
                            e.printStackTrace()
                        }catch(e: InvocationTargetException){
                            e.printStackTrace()
                        }

                        if(geoMatches != null)
                            callback.onNewLocationAviable(geoMatches[0].getAddressLine(0))
                        else
                            callback.onNewLocationAviable("Posizione non trovata")
                    }
                }

                override fun onStatusChanged(provider: String?, status: Int, extras: Bundle?) {}
                override fun onProviderEnabled(provider: String?) {}
                override fun onProviderDisabled(provider: String?) {}},null)
    }
}