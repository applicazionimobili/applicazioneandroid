package com.example.stuffexchange.view.list

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RatingBar
import android.widget.Toast
import androidx.activity.addCallback
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController

import com.example.stuffexchange.R
import com.example.stuffexchange.model.OggettoAttivo
import com.example.stuffexchange.model.UtenteChat
import com.example.stuffexchange.util.Caricamento
import com.example.stuffexchange.util.Connessione
import com.example.stuffexchange.view.MainActivity
import com.example.stuffexchange.viewmodel.OggettiViewModel
import com.example.stuffexchange.viewmodel.ViewModelFactory
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_oggetto_attivo_dettagli.*


class OggettoAttivoDettagliFragment : Fragment(), RatingBar.OnRatingBarChangeListener {

    private lateinit var viewModelFactory: ViewModelFactory
    private lateinit var viewModel: OggettiViewModel
    private lateinit var connessione: Connessione
    private lateinit var caricamento: Caricamento
    private lateinit var mainActivity: MainActivity
    private var codiceDestinazione = 0
    private val CODICE_PRESTATO = 1
    private val CODICE_IN_PRESTITO = 2

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        viewModelFactory = ViewModelFactory(requireActivity().application)
        viewModel = ViewModelProvider(this, viewModelFactory).get(OggettiViewModel::class.java)
        connessione = Connessione(requireActivity())
        caricamento = Caricamento(requireActivity())
        mainActivity = requireActivity() as MainActivity

        return inflater.inflate(R.layout.fragment_oggetto_attivo_dettagli, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        ratingBar.onRatingBarChangeListener = this
        mainActivity.toolbar.title = "Dettaglio oggetto"

        arguments?.let {
            val oggetto =  OggettoAttivoDettagliFragmentArgs.fromBundle(it).oggettoAttivo
            codiceDestinazione = OggettoAttivoDettagliFragmentArgs.fromBundle(it).codiceDestinazione
            aggiornamentoDettaglioOggettoAttivo(oggetto)

            requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner){
                when(codiceDestinazione){
                    CODICE_PRESTATO->{
                        val navController = findNavController()

                        if(navController.currentDestination?.id == R.id.oggettoAttivoDettagliFragment)
                            navController.navigate(R.id.action_oggettoAttivoDettagliFragment_to_oggettiPrestatiFragment)
                    }

                    CODICE_IN_PRESTITO->{
                        val navController = findNavController()

                        if(navController.currentDestination?.id == R.id.oggettoAttivoDettagliFragment)
                            navController.navigate(R.id.action_oggettoAttivoDettagliFragment_to_oggettiPresiInPrestitoFragment)
                    }
                }
            }
        }

        button.setOnClickListener {
            recensioneUtente()
        }

        bottoneChat.setOnClickListener {
            val oggettoAttivo = viewModel.getOggettoAttivo().value
            val utenteChat = UtenteChat(oggettoAttivo?.nomeUtente, oggettoAttivo?.idUtente)
            val action = OggettoAttivoDettagliFragmentDirections.actionOggettoAttivoDettagliFragmentToMessaggiFragment(utenteChat)
            findNavController().navigate(action)
        }
    }

    override fun onResume() {
        super.onResume()
        osservaCaricamento()
        osservaRecensioneUtente()
    }

    override fun onStop() {
        super.onStop()
        caricamento.dismissDialog()
    }

    private fun aggiornamentoDettaglioOggettoAttivo(oggetto: OggettoAttivo) {
        if(!connessione.controlloConnessione()){
            Toast.makeText(requireActivity(), "Connessione Internet assente", Toast.LENGTH_SHORT).show()
            return
        }

        viewModel.aggiornamentoDettaglioOggettoAttivo(oggetto).observe(viewLifecycleOwner, Observer {
            it?.let {
                nome.text = it.nome
                posizione.text = it.posizione
                utente.text = it.nomeUtente
                startDate.text = it.inizioPrestito
                endDate.text = it.finePrestito
                Picasso.get().load(it.imgUrl).fit().placeholder(R.drawable.caricamento).into(imageView)

                if(it.stato == "expired"){
                    textView.visibility = View.VISIBLE
                    ratingBar.visibility = View.VISIBLE
                    button.visibility = View.VISIBLE
                }
            }
        })
    }

    private fun recensioneUtente() {
        if (!connessione.controlloConnessione()) {
            Toast.makeText(activity, "Connessione internet assente", Toast.LENGTH_SHORT).show()
            return
        }

        viewModel.recensioneUtente(codiceDestinazione)?.observe(viewLifecycleOwner, Observer { eventWrapper->
            val result = eventWrapper.getContentIfNotHandled()
            azioneRecensioneUtente(result)
        })
    }

    private fun osservaRecensioneUtente(){
        viewModel.getSuccessRecensione()?.observe(viewLifecycleOwner, Observer {eventWrapper->
            val result = eventWrapper.getContentIfNotHandled()
            azioneRecensioneUtente(result)
        })
    }

    private fun azioneRecensioneUtente(result: Boolean?){
        result?.let {
            if(it){
                viewModel.setCaricamento(false)
                val action = OggettoAttivoDettagliFragmentDirections.actionOggettoAttivoDettagliFragmentToCaricamentoCompletato(codiceDestinazione)
                findNavController().navigate(action)
            }
            else {
                viewModel.setCaricamento(false)
                Toast.makeText(requireActivity(), "Errore durante la recensione", Toast.LENGTH_SHORT).show()
            }

        }
    }

    override fun onRatingChanged(ratingBar: RatingBar?, rating: Float, fromUser: Boolean) {
        viewModel.setRating(rating)
    }

    private fun osservaCaricamento() {
        viewModel.getCaricamento().observe(viewLifecycleOwner, Observer {
            if(it)
                caricamento.loadingAlertDialog()
            else
                caricamento.dismissDialog()
        })
    }

}
