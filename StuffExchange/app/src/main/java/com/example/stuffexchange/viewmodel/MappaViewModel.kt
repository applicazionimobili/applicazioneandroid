package com.example.stuffexchange.viewmodel

import android.annotation.SuppressLint
import android.app.Application
import android.location.Address
import android.location.Geocoder
import android.widget.TextView
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.stuffexchange.model.EventWrapper
import com.example.stuffexchange.model.Oggetto
import com.example.stuffexchange.model.OggettoAttivo
import com.example.stuffexchange.repository.MappaRepository
import com.google.android.gms.maps.model.LatLng
import java.io.IOException
import java.lang.reflect.InvocationTargetException
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class MappaViewModel(application: Application): ViewModel() {
    private val mappaRepository: MappaRepository = MappaRepository(application)

    private var latLng = MutableLiveData<LatLng>()
    private var listaOggetti = MutableLiveData<ArrayList<Oggetto>>()
    private var oggetto = MutableLiveData<Oggetto>()
    private var messaggio = MutableLiveData<String>()
    private var caricamento = MutableLiveData(false)
    private lateinit var successPrenotazione : LiveData<EventWrapper<Boolean>>
    private lateinit var datePrenotate: LiveData<ArrayList<String>>
    private var startDate: String? = null
    private var endDate: String? = null

    fun setCaricamento(caricamento: Boolean){
        this.caricamento.value = caricamento
    }

    fun setStartDate(startDate: String?) {
        this.startDate = startDate
    }

    fun setEndDate(endDate: String?) {
        this.endDate = endDate
    }

    fun getStartDate() : String? = startDate
    fun getEndDate() : String? = endDate
    fun getCaricamento() : LiveData<Boolean> = caricamento
    fun getMessaggio() : LiveData<String> = messaggio
    fun getLatLng(): LiveData<LatLng> = latLng
    fun getListaOggetti(): LiveData<ArrayList<Oggetto>> = listaOggetti
    fun getGiorno():Int = giorno
    fun getMese():Int = mese
    fun getAnno():Int = anno
    fun getOggetto(): MutableLiveData<Oggetto> = oggetto

    private var calendario = Calendar.getInstance()
    private var giorno = 0
    private var mese = 0
    private var anno = 0


    fun aggiornaDatiMappa(array: Array<Oggetto>, posizione: String, geocoder: Geocoder) {
        val aux = ArrayList<Oggetto>()

        array.forEach {
            aux.add(it)
        }

        listaOggetti.value = aux

        val geoMatches: List<Address>?

        try {
            geoMatches = geocoder.getFromLocationName(posizione, 1)
        }catch (e: IOException){
            e.printStackTrace()
            return
        }catch (e: InvocationTargetException){
            e.printStackTrace()
            return
        }

        if(geoMatches != null){
            latLng.value = LatLng(geoMatches[0].latitude, geoMatches[0].longitude)
        }
    }

    fun aggiornaLista(array: Array<Oggetto>): LiveData<ArrayList<Oggetto>> {
        val aux = ArrayList<Oggetto>()

        array.forEach {
            aux.add(it)
        }

        listaOggetti.value = aux
        return listaOggetti
    }

    fun aggiornaDettaglio(oggetto: Oggetto): MutableLiveData<Oggetto> {
        this.oggetto.value = oggetto
        return this.oggetto
    }

    fun selezionaData(data: TextView?) {
        if(data?.text.toString().isEmpty()) {
            giorno = calendario.get(Calendar.DAY_OF_MONTH)
            mese = calendario.get(Calendar.MONTH) + 1
            anno = calendario.get(Calendar.YEAR)
        }
        else{
            val aux = data?.text.toString().split("/")
            giorno = aux[0].toInt()
            mese = aux[1].toInt()
            anno = aux[2].toInt()
        }
    }

    fun prenotaOggetto(startDate: TextView, endDate: TextView): LiveData<EventWrapper<Boolean>>? {
        caricamento.value = true
        val oggettoAttivo = OggettoAttivo(
            "",
            "",
            oggetto.value?.id,
            oggetto.value?.imgUrl,
            oggetto.value?.nome,
            oggetto.value?.posizione,
            startDate.text.toString(),
            endDate.text.toString(),
            "in progress")
        successPrenotazione = mappaRepository.prenotaOggetto(oggettoAttivo, oggetto.value?.idVenditore)
        return successPrenotazione
    }

    @SuppressLint("SimpleDateFormat")
    fun controlloDatiPrenotazione(startDateText: String, endDateText: String): Boolean {
        if(startDateText.isEmpty() || endDateText.isEmpty()) {
            messaggio.value = "Inserire delle date valide"
            return false
        }

        val sdf = SimpleDateFormat("dd/M/yyyy")
        val myStartDate = sdf.parse(startDateText)
        val myEndDate = sdf.parse(endDateText)
        val objStartDate = sdf.parse(oggetto.value?.startData!!)
        val objEndDate = sdf.parse(oggetto.value?.endData!!)

        if(myStartDate!!<objStartDate || myEndDate!!>objEndDate) {
            messaggio.value = "Oggetto non prenotabile in queste date"
            return false
        }

        if(myEndDate < myStartDate){
            messaggio.value = "La data di fine non può essere precedente della data attuale"
            return false
        }

        datePrenotate.value?.forEach {
            val arrayDate = it.split("-")
            val startDate = sdf.parse(arrayDate[0])
            val endDate = sdf.parse(arrayDate[1])

            if(myStartDate in startDate..endDate) {
                messaggio.value = "Oggetto già prenotato in queste date"
                return false
            }

            if(myEndDate in startDate..endDate) {
                messaggio.value = "Oggetto già prenotato in queste date"
                return false
            }

            if(myStartDate<=startDate && myEndDate>=endDate) {
                messaggio.value = "Oggetto già prenotato in queste date"
                return false
            }
        }

        return true
    }

    fun aggiornaDatePrenotate(): LiveData<ArrayList<String>> {
        datePrenotate = mappaRepository.aggiornateDatePrenotate(oggetto.value)
        return datePrenotate
    }

    fun getSuccessPrenotazione() : LiveData<EventWrapper<Boolean>>? {
        return if(this::successPrenotazione.isInitialized)
            successPrenotazione
        else
            null
    }
}