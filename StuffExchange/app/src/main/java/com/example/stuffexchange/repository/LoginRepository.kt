package com.example.stuffexchange.repository

import android.annotation.SuppressLint
import android.app.Application
import android.os.AsyncTask
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.stuffexchange.data.FirebaseDatabase
import com.example.stuffexchange.data.LocalDatabase
import com.example.stuffexchange.model.*
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.gms.tasks.OnFailureListener
import com.google.android.gms.tasks.OnSuccessListener
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.AuthResult
import com.google.firebase.firestore.DocumentSnapshot
import com.google.firebase.firestore.QuerySnapshot
import java.text.SimpleDateFormat
import java.util.*

@Suppress("DEPRECATION")
class LoginRepository(application: Application) {
    private var firebaseDatabase: FirebaseDatabase = FirebaseDatabase.instance
    private var localDatabase: LocalDatabase = LocalDatabase.getInstance(application)

    fun checkCurrent(): LiveData<EventWrapper<Boolean>> {
        val success = MutableLiveData<EventWrapper<Boolean>>()
        success.value = EventWrapper(firebaseDatabase.checkCurrent())
        return success
    }

    fun login(email: String, password: String):  LiveData<EventWrapper<Boolean>>{
        val success = MutableLiveData<EventWrapper<Boolean>>()

        val loginCompleteListener = OnCompleteListener<AuthResult>{
            if(it.isSuccessful)
                success.value = EventWrapper(true)
            else
                success.value = EventWrapper(false)
        }

        firebaseDatabase.login(email, password, loginCompleteListener)
        return success
    }

    fun resetPassword(email: String):LiveData<EventWrapper<Boolean>> {
        val success = MutableLiveData<EventWrapper<Boolean>>()

        val resetPasswordCompleteListener = OnCompleteListener<Void>{
            if(it.isSuccessful)
                success.value = EventWrapper(true)
            else
                success.value = EventWrapper(false)
        }

        firebaseDatabase.resetPassword(email, resetPasswordCompleteListener)
        return success
    }

    fun registrazione(utente: Utente, password: String): LiveData<EventWrapper<Boolean>> {
        val successRegistration = MutableLiveData<EventWrapper<Boolean>>()

        val registrazioneSuccessListener = OnSuccessListener<Void>{
            successRegistration.value = EventWrapper(true)
        }

        val registrazioneFailureListener = OnFailureListener{
            successRegistration.value = EventWrapper(false)
        }

        firebaseDatabase.registrazione(utente, password, registrazioneSuccessListener, registrazioneFailureListener)
        return successRegistration
    }

    fun aggiornamentoDatiUtente(): LiveData<EventWrapper<Utente>> {
        val success = MutableLiveData<EventWrapper<Utente>>()

        val utenteSuccessListener = OnSuccessListener<DocumentSnapshot>{document->
            document.data?.let {
                val utente = document.toObject(Utente::class.java)

                utente?.let {
                    inserireUtenteTask(localDatabase, utente, success).execute()
                }
            }
        }

        val utentFailureListener = OnFailureListener {
            success.value = EventWrapper(Utente())
        }

        firebaseDatabase.aggiornamentoDatiUtente(utenteSuccessListener, utentFailureListener)
        return success
    }

    class inserireUtenteTask(private var localDatabase: LocalDatabase,
                             private var utente: Utente,
                             private val success: MutableLiveData<EventWrapper<Utente>>): AsyncTask<Utente, Void, Void>()
    {
        override fun doInBackground(vararg params: Utente?): Void? {
            localDatabase.getUtenteDAO().insertUtente(utente)
            return null
        }

        override fun onPostExecute(result: Void?) {
            super.onPostExecute(result)
            success.value = EventWrapper(utente)
        }
    }

    /**
     * La funzione ha il compito di controllare all'ingresso dell'utente se un suo oggetto caricato
     * prestato o presto in prestito sia scaduto, cioè la sua data di scadenza sia minore della data
     * attuale.
     * Ordine di esecuzioni delle funzioni:
     * - firebaseDatabase.controlloPrestatiScaduti(prestatiSuccesListener, scadutiFailureLister)
     * - firebaseDatabase.controlloPrestatiScaduti(prestatiSuccesListener, scadutiFailureLister)
     * - firebaseDatabase.controlloInPrestitoScaduti(inPrestitoSuccessListener, scadutiFailureLister)
     * - firebaseDatabase.aggiornaStato(arrayCaricati, arrayPrestati, arrayInPrestito, scadutiSuccessListener, scadutiFailureLister)
     *
     */
    @SuppressLint("SimpleDateFormat")
    fun controllaOggettiScaduti(dataOdierna: Date): LiveData<EventWrapper<NotificaOggetto>> {
        val sdf = SimpleDateFormat("dd/M/yyyy")
        val success = MutableLiveData<EventWrapper<NotificaOggetto>>()
        val arrayCaricati = ArrayList<Oggetto>()
        val arrayPrestati = ArrayList<OggettoAttivo>()
        val arrayInPrestito = ArrayList<OggettoAttivo>()


        /*In caso di errore success viene impostato a null*/
        val scadutiFailureLister = OnFailureListener{
            success.value = null
        }

        /*Viene creato l'oggetto notifica e i campi vengono impostati a true in base alla presenza
        * di oggetti scaduti nell'array caricati, prestati e presi in prestito
        * In seguito viene impostato il success per lanciare la notifica*/
         val scadutiSuccessListener = OnSuccessListener<List<Task<*>>>{
             val notifica = NotificaOggetto(false, false, false)

             if(arrayCaricati.isNotEmpty() && arrayCaricati.size > 0)
                 notifica.caricato = true

             if(arrayPrestati.isNotEmpty() && arrayPrestati.size > 0)
                 notifica.prestato = true

             if(arrayInPrestito.isNotEmpty() && arrayInPrestito.size > 0)
                 notifica.inPrestito = true

             success.value = EventWrapper(notifica)

        }

        /*Cicla gli oggetti presi in prestito e aggiunge quelli scaduti ad arrayInPrestito, poi chiama la funzione
        per controllare lo stato degli oggetti*/
        val inPrestitoSuccessListener = OnSuccessListener<QuerySnapshot>{documenti->
            documenti?.forEach {
                val oggetto = it.toObject(OggettoAttivo::class.java)
                val endDate = sdf.parse(oggetto.finePrestito!!)
                endDate!!.hours = 1

                if(endDate < dataOdierna && oggetto.stato == "in progress")
                    arrayInPrestito.add(oggetto)
            }

            /*Aggiorna lo stato degli oggetti presenti negli array (arrayPrestati, arrayInPrestito,
            * arrayCaricati) da in progress a expired*/
            firebaseDatabase.aggiornaStato(arrayCaricati, arrayPrestati, arrayInPrestito, scadutiSuccessListener, scadutiFailureLister)
        }

        /*Cicla gli oggetti prestati e aggiunge quelli scaduti ad arrayPrestati, poi chiama la funzione
        per controllare gli oggetti presi in prestito*/
        val prestatiSuccesListener = OnSuccessListener<QuerySnapshot>{documenti->
            documenti?.forEach {
                val oggetto = it.toObject(OggettoAttivo::class.java)
                val endDate = sdf.parse(oggetto.finePrestito!!)
                endDate!!.hours = 1

                if(endDate < dataOdierna  && oggetto.stato == "in progress")
                    arrayPrestati.add(oggetto)
            }

            /*Recupera gli oggetti presi in prestito*/
           firebaseDatabase.controlloInPrestitoScaduti(inPrestitoSuccessListener, scadutiFailureLister)
        }

        /*Cicla gli oggetti caricati e aggiunge quelli scaduti ad arrayCaricati, poi chiama la funzione
        per controllare gli oggetti prestati*/
        val caricatiSuccessListener = OnSuccessListener<QuerySnapshot>{documenti->
            documenti?.forEach {
                val oggetto = it.toObject(Oggetto::class.java)
                val endDate = sdf.parse(oggetto.endData!!)
                endDate!!.hours = 1

                if(endDate< dataOdierna  && oggetto.stato == "in progress")
                    arrayCaricati.add(oggetto)
            }

            /*Recupera gli oggetti prestati*/
            firebaseDatabase.controlloPrestatiScaduti(prestatiSuccesListener, scadutiFailureLister)
        }


        /*Recupera gli oggetti caricati*/
        firebaseDatabase.controlloCaricatiScaduti(caricatiSuccessListener, scadutiFailureLister)
        return success
    }

    fun logout() {
        firebaseDatabase.logout()
    }
}