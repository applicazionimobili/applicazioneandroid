package com.example.stuffexchange.data

import android.net.Uri
import androidx.lifecycle.MutableLiveData
import com.example.stuffexchange.model.*
import com.google.android.gms.tasks.*
import com.google.firebase.auth.AuthResult
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.firestore.*
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference

class FirebaseDatabase(private var auth: FirebaseAuth = FirebaseAuth.getInstance(),
                       db: FirebaseFirestore = FirebaseFirestore.getInstance(),
                       private var current: FirebaseUser? = auth.currentUser,
                       private var storageReference: StorageReference = FirebaseStorage.getInstance().reference) {

    private object HOLDER{
        val instance = FirebaseDatabase()
    }

    companion object {
        val instance : FirebaseDatabase by lazy {
            HOLDER.instance
        }
    }

    private var utenteRef = db.collection("Dati utente")
    private var chatRef = db.collection("Chat")



    fun login(email: String, password: String, loginCompleteListener: OnCompleteListener<AuthResult>) {
        auth.signInWithEmailAndPassword(email, password)
            .addOnCompleteListener(loginCompleteListener)
    }

    fun checkCurrent(): Boolean {
        return current != null
    }

    fun resetPassword(email: String, resetPasswordCompleteListener: OnCompleteListener<Void>) {
        auth.sendPasswordResetEmail(email)
            .addOnCompleteListener(resetPasswordCompleteListener)
    }

    fun registrazione(
        utente: Utente,
        password: String,
        registrazioneSuccessListener : OnSuccessListener<Void>,
        registrazioneFailureListener : OnFailureListener
    ) {
        auth.createUserWithEmailAndPassword(utente.mail, password).addOnSuccessListener {
            utente.id = auth.uid.toString()
            utenteRef.document(auth.uid.toString()).set(utente)
                .addOnSuccessListener(registrazioneSuccessListener)
                .addOnFailureListener(registrazioneFailureListener)
        }.addOnFailureListener(registrazioneFailureListener)
    }

    fun aggiornamentoDatiUtente(
        utenteSuccessListener: OnSuccessListener<DocumentSnapshot>,
        utentFailureListener: OnFailureListener
    ) {
        utenteRef.document(auth.uid.toString()).get()
            .addOnSuccessListener(utenteSuccessListener)
            .addOnFailureListener(utentFailureListener)
    }

    fun logout() {
        auth.signOut()
        current = null
    }

    fun caricaOggetto(oggetto: Oggetto, imageUri: Uri?, caricaOggettoSuccessListener: OnSuccessListener<Void>, caricaOggettoFailureListener: OnFailureListener) {
        val id = auth.uid.toString()
        var idOggetto : String

        utenteRef.document(id).get().addOnSuccessListener {
            val utente = it.toObject(Utente::class.java)
            val numeroOperazioni = utente?.numeroOperazioni
            idOggetto = "${id}_${oggetto.nome}_#$numeroOperazioni"
            val immagineRef: StorageReference = storageReference.child("$id/${oggetto.nome}_#$numeroOperazioni.png")
            oggetto.id = idOggetto
            oggetto.idVenditore = id
            oggetto.nomeVenditore = "${utente?.nome} ${utente?.cognome}"

            utenteRef.document(id).update("numeroOperazioni", FieldValue.increment(1)).addOnSuccessListener {
                    immagineRef.putFile(imageUri!!).addOnSuccessListener {
                        immagineRef.downloadUrl.addOnSuccessListener {url->
                            oggetto.imgUrl = url.toString()
                            oggetto.id = idOggetto
                            val oggettoRef = utenteRef.document(id).collection("Oggetti Caricati")
                            oggettoRef.document(idOggetto).set(oggetto)
                                .addOnSuccessListener(caricaOggettoSuccessListener)
                                .addOnFailureListener(caricaOggettoFailureListener)
                        }.addOnFailureListener(caricaOggettoFailureListener)
                    }.addOnFailureListener(caricaOggettoFailureListener)
                }.addOnFailureListener(caricaOggettoFailureListener)
        }.addOnFailureListener(caricaOggettoFailureListener)
    }

    fun scaricaListaOggettiCaricati(
        oggettiCaricatiSuccessListener: OnSuccessListener<QuerySnapshot>,
        oggettiCaricatiFailureListener: OnFailureListener
    ) {
        val id = auth.uid.toString()

        utenteRef.document(id).collection("Oggetti Caricati").get()
            .addOnSuccessListener(oggettiCaricatiSuccessListener)
            .addOnFailureListener(oggettiCaricatiFailureListener)
    }

    fun aggiornaListaUtenti(listaUtentiSuccessListener: OnSuccessListener<QuerySnapshot>, listaUtentiFailureListener: OnFailureListener) {
        utenteRef.get()
            .addOnSuccessListener(listaUtentiSuccessListener)
            .addOnFailureListener(listaUtentiFailureListener)
    }

    fun eliminaOggetto(
        idOggetto: String,
        eliminaOggettoSuccessListener: OnSuccessListener<Void>,
        eliminaOggettoFailureListener: OnFailureListener) {

        val id = auth.uid.toString()
        utenteRef.document(id).collection("Oggetti Caricati").document(idOggetto).delete()
            .addOnSuccessListener(eliminaOggettoSuccessListener)
            .addOnFailureListener(eliminaOggettoFailureListener)
    }

    fun getChat(recuperaUtenteSuccessListener: OnSuccessListener<QuerySnapshot>, recuperaUtenteFailureListener: OnFailureListener) {
        chatRef.get()
            .addOnSuccessListener(recuperaUtenteSuccessListener)
            .addOnFailureListener(recuperaUtenteFailureListener)
    }

    fun aggiornaListaChat(listaUtentiSuccessListener: OnSuccessListener<QuerySnapshot>, listaUtentiFailureListener: OnFailureListener) {
        chatRef.get()
            .addOnSuccessListener(listaUtentiSuccessListener)
            .addOnFailureListener(listaUtentiFailureListener)

    }

    private var listener: ListenerRegistration? = null

    fun aggiungiListener(id: String, listaMessaggi: MutableLiveData<ArrayList<Messaggio>>) {
        if(listener == null) {
            chatRef.document(id).collection("Messaggi").addSnapshotListener { querySnapshot, e ->
                if (e != null) {
                    return@addSnapshotListener
                }

                if (querySnapshot != null) {
                    val aux = ArrayList<Messaggio>()

                    querySnapshot.forEach {
                        val messaggio = it.toObject(Messaggio::class.java)
                        messaggio.idUtente = auth.uid.toString()
                        aux.add(messaggio)
                    }

                    listaMessaggi.value = aux
                }
            }
        }
    }

    fun inviaMessaggio(
        idChat: String,
        messaggio: Messaggio,
        inviaMessaggioSuccessListener: OnSuccessListener<Void>,
        inviaMessaggioFailureListener: OnFailureListener
    ) {
        chatRef.document(idChat).get().addOnSuccessListener {
            val numeroMessaggio = it.data?.get("numeroMessaggi").toString().toInt()
            chatRef.document(idChat).update("numeroMessaggi", FieldValue.increment(1)).addOnSuccessListener {
                val idMessaggio = "${idChat}_#$numeroMessaggio"
                chatRef.document(idChat).collection("Messaggi").document(idMessaggio).set(messaggio)
                    .addOnSuccessListener(inviaMessaggioSuccessListener)
                    .addOnFailureListener(inviaMessaggioFailureListener)
            }.addOnFailureListener(inviaMessaggioFailureListener)
        }.addOnFailureListener(inviaMessaggioFailureListener)
    }

    fun creaChat(
        chat: Chat,
        messaggio: Messaggio,
        inviaMessaggioSuccessListener: OnSuccessListener<Void>,
        inviaMessaggioFailureListener: OnFailureListener
    ) {
        val idChat = "${chat.idMittente}_${chat.idDestinatario}"

        chatRef.document(idChat).set(chat).addOnSuccessListener {
           inviaMessaggio(
               idChat,
               messaggio,
               inviaMessaggioSuccessListener,
               inviaMessaggioFailureListener
           )
        }.addOnFailureListener(inviaMessaggioFailureListener)
    }


    fun stopListener() {
        listener?.remove()
        listener = null
    }

    fun prenotaOggetto(
        oggettoAttivo: OggettoAttivo,
        compratore: Utente,
        idVenditore: String,
        prenotaOggettoSuccessListener: OnSuccessListener<Void>,
        prenotaOggettoFailureListener: OnFailureListener) {

        var venditore: Utente?

        //recupero il venditore
        utenteRef.document(idVenditore).get().addOnSuccessListener{documentoVenditore->
            venditore = documentoVenditore.toObject(Utente::class.java)
            oggettoAttivo.idOggetto += "_prestato_#${venditore?.numeroOperazioni}"
            oggettoAttivo.idUtente = compratore.id
            oggettoAttivo.nomeUtente = "${compratore.nome} ${compratore.cognome}"
            //aggiorna il numero delle operazioni del venditore
            utenteRef.document(idVenditore).update("numeroOperazioni", FieldValue.increment(1)).addOnSuccessListener{
                //inserisco l'oggetto prestato nella collection Oggetti prestati del venditore
                utenteRef.document(idVenditore).collection("Oggetti prestati").document(oggettoAttivo.idOggetto!!).set(oggettoAttivo).addOnSuccessListener {

                    //recupero il compratore
                        oggettoAttivo.idOggetto += "_prenotato_#${compratore.numeroOperazioni}"
                        oggettoAttivo.idUtente = compratore.id
                        //recupero il venditore per impostare nell'oggetto ID e nome del venditore
                            oggettoAttivo.idUtente = idVenditore
                            oggettoAttivo.nomeUtente = "${venditore?.nome} ${venditore?.cognome}"
                            //aggiorna numero operazioni compratore
                            utenteRef.document(compratore.id).update("numeroOperazioni", FieldValue.increment(1)).addOnSuccessListener{
                                //aggiungo oggetto alla collection Oggetti presi in prestito del compratore
                                utenteRef.document(compratore.id).collection("Oggetti presi in prestito").document(oggettoAttivo.idOggetto!!).set(oggettoAttivo)
                                    .addOnSuccessListener(prenotaOggettoSuccessListener)
                                    .addOnFailureListener(prenotaOggettoFailureListener)
                            }.addOnFailureListener(prenotaOggettoFailureListener)
                }.addOnFailureListener(prenotaOggettoFailureListener)
            }.addOnFailureListener(prenotaOggettoFailureListener)
        }.addOnFailureListener(prenotaOggettoFailureListener)
    }

    fun aggiornaDatePrenotate(
        oggeto: Oggetto,
        datePrenotateSuccessListener: OnSuccessListener<QuerySnapshot>,
        datePrenotateFailureListener: OnFailureListener) {

        utenteRef.document(oggeto.idVenditore!!).collection("Oggetti prestati").get()
            .addOnSuccessListener(datePrenotateSuccessListener)
            .addOnFailureListener(datePrenotateFailureListener)
    }

    fun aggiornaListaInPrestito(oggettiPresiInPrestitoSuccessListener: OnSuccessListener<QuerySnapshot>, oggettiPresiInPrestitoFailureListener: OnFailureListener) {

        utenteRef.document(auth.uid.toString()).collection("Oggetti presi in prestito").get()
            .addOnSuccessListener(oggettiPresiInPrestitoSuccessListener)
            .addOnFailureListener(oggettiPresiInPrestitoFailureListener)
    }

    fun aggiornaListaPrestati(
        oggettiPrestatiSuccessListener: OnSuccessListener<QuerySnapshot>,
        oggettiPrestatiFailureListener: OnFailureListener) {

        utenteRef.document(auth.uid.toString()).collection("Oggetti prestati").get()
            .addOnSuccessListener(oggettiPrestatiSuccessListener)
            .addOnFailureListener(oggettiPrestatiFailureListener)
    }

    fun recensioneUtente(
        aggiornamentoSuccesListener: OnSuccessListener<DocumentSnapshot>,
        recensioneFailureLister: OnFailureListener,
        oggettoAttivo: OggettoAttivo
    ) {
        utenteRef.document(oggettoAttivo.idUtente!!).get()
            .addOnSuccessListener(aggiornamentoSuccesListener)
            .addOnFailureListener(recensioneFailureLister)
    }

    fun aggiornaRecensione(
        nuovoNumeroVoti: Int,
        nuovoVotoMedio: Float,
        recensioneSuccessListener: OnSuccessListener<Void>,
        recensioneFailureListener: OnFailureListener,
        oggettoAttivo: OggettoAttivo,
        codiceDestinazione: Int
    ) {
        utenteRef.document(oggettoAttivo.idUtente!!).update("votoMedio", nuovoVotoMedio).addOnSuccessListener {
                utenteRef.document(oggettoAttivo.idUtente!!).update("numeroVoti", nuovoNumeroVoti).addOnSuccessListener {
                    var collezione = ""

                    if(codiceDestinazione == 1)
                        collezione = "Oggetti prestati"
                    else if(codiceDestinazione == 2)
                        collezione = "Oggetti presi in prestito"

                    utenteRef.document(auth.uid.toString()).collection(collezione).document(oggettoAttivo.idOggetto!!).update("stato", "complete")
                        .addOnSuccessListener(recensioneSuccessListener)
                        .addOnFailureListener(recensioneFailureListener)
                }.addOnFailureListener(recensioneFailureListener)
        }.addOnFailureListener(recensioneFailureListener)
    }

    fun controlloCaricatiScaduti(caricatiSuccessListener: OnSuccessListener<QuerySnapshot>, scadutiFailureLister: OnFailureListener) {
        utenteRef.document(auth.uid.toString()).collection("Oggetti Caricati").get()
            .addOnSuccessListener(caricatiSuccessListener)
            .addOnFailureListener(scadutiFailureLister)
    }

    fun controlloPrestatiScaduti(caricatiSuccessListener: OnSuccessListener<QuerySnapshot>, scadutiFailureLister: OnFailureListener) {
        utenteRef.document(auth.uid.toString()).collection("Oggetti prestati").get()
            .addOnSuccessListener(caricatiSuccessListener)
            .addOnFailureListener(scadutiFailureLister)
    }

    fun controlloInPrestitoScaduti(caricatiSuccessListener: OnSuccessListener<QuerySnapshot>, scadutiFailureLister: OnFailureListener) {
        utenteRef.document(auth.uid.toString()).collection("Oggetti presi in prestito").get()
            .addOnSuccessListener(caricatiSuccessListener)
            .addOnFailureListener(scadutiFailureLister)
    }



    fun aggiornaStato(
        arrayCaricati: ArrayList<Oggetto>,
        arrayPrestati: java.util.ArrayList<OggettoAttivo>,
        arrayInPrestito: java.util.ArrayList<OggettoAttivo>,
        scadutiSuccessListener: OnSuccessListener<List<Task<*>>>,
        scadutiFailureLister: OnFailureListener
    ) {
        val oggettiCaricati = ArrayList<Task<Void>>()

        arrayCaricati.forEach {
            val task = utenteRef.document(auth.uid.toString()).collection("Oggetti Caricati").document(it.id!!).update("stato", "expired")
            oggettiCaricati.add(task)
        }

        arrayPrestati.forEach {
            val task = utenteRef.document(auth.uid.toString()).collection("Oggetti prestati").document(it.idOggetto!!).update("stato", "expired")
            oggettiCaricati.add(task)
        }

        arrayInPrestito.forEach {
            val task = utenteRef.document(auth.uid.toString()).collection("Oggetti presi in prestito").document(it.idOggetto!!).update("stato", "expired")
            oggettiCaricati.add(task)
        }

        Tasks.whenAllComplete(oggettiCaricati).
            addOnSuccessListener(scadutiSuccessListener)
            .addOnFailureListener(scadutiFailureLister)
    }

    fun cercaOggetti(utenti: ArrayList<Utente>,
                     oggettiSuccessListener: OnSuccessListener<QuerySnapshot>,
                     ricercaSuccessListener: OnSuccessListener<List<QuerySnapshot>>,
                     ricercaFailureListener: OnFailureListener) {

        val tasks = ArrayList<Task<QuerySnapshot>>()

        utenti.forEach {
            val task = utenteRef.document(it.id).collection("Oggetti Caricati").get()
                .addOnSuccessListener(oggettiSuccessListener)

            tasks.add(task)
        }

        Tasks.whenAllSuccess<QuerySnapshot>(tasks)
            .addOnSuccessListener(ricercaSuccessListener)
            .addOnFailureListener(ricercaFailureListener)
    }
}