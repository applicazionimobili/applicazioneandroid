package com.example.stuffexchange.view.login

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.example.stuffexchange.R
import com.example.stuffexchange.model.NotificaOggetto
import com.example.stuffexchange.model.Utente
import com.example.stuffexchange.util.Connessione
import com.example.stuffexchange.util.Notifiche
import com.example.stuffexchange.view.MainActivity
import com.example.stuffexchange.viewmodel.LoginViewModel
import com.example.stuffexchange.viewmodel.ViewModelFactory


class SplashFragment : Fragment() {

    private lateinit var viewModelFactory: ViewModelFactory
    private lateinit var viewModel: LoginViewModel
    private lateinit var connessione: Connessione
    private lateinit var appCompatActivity: AppCompatActivity
    private lateinit var mainActivity: MainActivity
    private lateinit var notifica : Notifiche

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        mainActivity = requireActivity() as MainActivity
        viewModelFactory = ViewModelFactory(mainActivity.application)
        viewModel = ViewModelProvider(this, viewModelFactory).get(LoginViewModel::class.java)
        connessione = Connessione(requireActivity())
        notifica = Notifiche(requireActivity())
        appCompatActivity = requireActivity() as AppCompatActivity
        mainActivity.setDrawerLocked(true)
        return inflater.inflate(R.layout.fragment_splash, container, false)
    }

    override fun onStart() {
        super.onStart()
        osservaCurrent()
        osservaAggiornamentoUtente()
        osservaControlloOggettiScaduti()
        appCompatActivity.supportActionBar?.hide()

        if(!connessione.controlloConnessione()){
            Toast.makeText(requireActivity(), "Connessione assente", Toast.LENGTH_SHORT).show()
            val navController = findNavController()

            if(navController.currentDestination?.id == R.id.splashFragment)
                navController.navigate(R.id.action_splashFragment_to_loginFragment)

            return
        }

        checkCurrent()
    }

    private fun checkCurrent() {
        viewModel.checkCurrent().observe(viewLifecycleOwner, Observer { eventWrapper ->
            val result = eventWrapper.getContentIfNotHandled()
            azioniCurrent(result)
        })
    }

    private fun osservaCurrent() {
        viewModel.getSuccessCurrent()?.observe(viewLifecycleOwner, Observer { eventWrapper->
            val result = eventWrapper.getContentIfNotHandled()
            azioniCurrent(result)
        })
    }

    private fun azioniCurrent(result: Boolean?) {
        result?.let {
            if(it) {
                aggiornamentoDatiUtente()
            }
            else {
                val navController = findNavController()

                if(navController.currentDestination?.id == R.id.splashFragment)
                    navController.navigate(R.id.action_splashFragment_to_loginFragment)
            }
        }
    }

    private fun aggiornamentoDatiUtente() {
        viewModel.aggiornamentoDatiUtente()?.observe(viewLifecycleOwner, Observer { eventWrapper ->
            val result = eventWrapper.getContentIfNotHandled()
            azioniAggiornamentoUtente(result)
        })
    }

    private fun osservaAggiornamentoUtente() {
        viewModel.getsuccessAggiornamentoUtente()?.observe(viewLifecycleOwner, Observer {  eventWrapper ->
            val result = eventWrapper.getContentIfNotHandled()
            azioniAggiornamentoUtente(result)
        })
    }

    private fun azioniAggiornamentoUtente(result: Utente?) {
        result?.let {
            if(it.nome==""){
                viewModel.logout()
                Toast.makeText(requireActivity(), "Errore durante l'aggiornamento dei dati utente.\nRiprova il login", Toast.LENGTH_SHORT).show()
                val navController = findNavController()

                if(navController.currentDestination?.id == R.id.splashFragment)
                    navController.navigate(R.id.action_splashFragment_to_loginFragment)
            }

            mainActivity.aggiornamentoDrawer(it.nome, it.cognome, it.mail, it.votoMedio, it.numeroVoti)
            Toast.makeText(requireActivity(), "Dati utente aggiornati", Toast.LENGTH_SHORT).show()
            controllaOggettiScaduti()
        }
    }

    private fun controllaOggettiScaduti(){
        viewModel.controllaOggettiScaduti().observe(viewLifecycleOwner, Observer {eventWrapper->
            val result = eventWrapper.getContentIfNotHandled()
            azioniControlloOggettiScaduti(result)
        })
    }

    private fun osservaControlloOggettiScaduti() {
        viewModel.getControlloOggettiScaduti()?.observe(viewLifecycleOwner, Observer { eventWrapper ->
            if(eventWrapper == null) {
                Toast.makeText(mainActivity, "Non è stato possibile controllare gli oggetti scaduti, ti invitiamo a controllare eventuali scadenze", Toast.LENGTH_SHORT).show()
                viewModel.setCaricamento(false)
                mainActivity.setDrawerLocked(false)
                val navController = findNavController()

                if(navController.currentDestination?.id == R.id.splashFragment)
                    navController.navigate(R.id.action_splashFragment_to_searchFragment)
            }

            val result = eventWrapper.getContentIfNotHandled()
            azioniControlloOggettiScaduti(result)
        })
    }

    private fun azioniControlloOggettiScaduti(result: NotificaOggetto?) {
        result?.let {
            notifica.setupNotification()

            if(it.caricato){
                notifica.sendNotification("Controlla la lista degli oggetti caricati",1)
            }

            if(it.prestato){
                notifica.sendNotification("Controlla la lista degli oggetti prestati", 2)
            }

            if(it.inPrestito){
                notifica.sendNotification("Controlla la lista degli oggetti presi in prestito", 3)
            }

            viewModel.setCaricamento(false)
            mainActivity.setDrawerLocked(false)
            val navController = findNavController()

            if(navController.currentDestination?.id == R.id.splashFragment)
                findNavController().navigate(R.id.action_splashFragment_to_searchFragment)
        }
    }
}
