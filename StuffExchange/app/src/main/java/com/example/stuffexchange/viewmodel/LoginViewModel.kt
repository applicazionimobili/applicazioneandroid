package com.example.stuffexchange.viewmodel

import android.app.Application
import android.util.Patterns
import android.widget.EditText
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.stuffexchange.model.EventWrapper
import com.example.stuffexchange.model.NotificaOggetto
import com.example.stuffexchange.model.Utente
import com.example.stuffexchange.repository.LoginRepository
import com.example.stuffexchange.view.MainActivity
import java.util.*

@Suppress("DEPRECATION")
class LoginViewModel(application: Application) : ViewModel() {

    private var repository: LoginRepository = LoginRepository(application)

    private lateinit var successCurrent : LiveData<EventWrapper<Boolean>>
    private lateinit var successLogin : LiveData<EventWrapper<Boolean>>
    private lateinit var successReset : LiveData<EventWrapper<Boolean>>
    private lateinit var successRegistration : LiveData<EventWrapper<Boolean>>
    private lateinit var successAggiornamentoUtente : LiveData<EventWrapper<Utente>>
    private lateinit var successScaduti: LiveData<EventWrapper<NotificaOggetto>>
    private val caricamento = MutableLiveData(false)

    fun checkCurrent() : LiveData<EventWrapper<Boolean>>{
        successCurrent = repository.checkCurrent()
        return successCurrent
    }

    fun login(email: EditText, password: EditText): LiveData<EventWrapper<Boolean>> {
        caricamento.value = true
        successLogin = repository.login(email.text.toString(), password.text.toString())
        return successLogin
    }

    fun resetPassword(email: EditText) : LiveData<EventWrapper<Boolean>> {
        caricamento.value = true
        successReset = repository.resetPassword(email.text.toString())
        return successReset
    }

    fun registrazione(nome: EditText, cognome: EditText, email: EditText, password: EditText): LiveData<EventWrapper<Boolean>> {
        caricamento.value = true
        val utente = Utente(1,"", nome.text.toString(), cognome.text.toString(), email.text.toString(), 0, 0, 0.0.toFloat())
        successRegistration = repository.registrazione(utente, password.text.toString())
        return successRegistration
    }

    fun aggiornamentoDatiUtente() : LiveData<EventWrapper<Utente>>?{
        caricamento.value = true
        successAggiornamentoUtente = repository.aggiornamentoDatiUtente()
        return successAggiornamentoUtente
    }

    fun controlloDatiLogin(email: EditText, password: EditText): Boolean {
        if (email.text.toString().isEmpty()) {
            email.error = "Inserire mail"
            email.requestFocus()
            return false
        }

        if (password.text.toString().isEmpty()) {
            password.error = "Inserire password"
            password.requestFocus()
            return false
        }

        return true
    }

   fun controlloDatiResetPassword(email: EditText) : Boolean{
        if(email.text.isEmpty()) {
            email.error = "Inserire mail valida"
            email.requestFocus()
            return false
        }

        return true
    }

    fun controlloDatiRegistrazione(name: EditText, surname: EditText, email: EditText, pw1: EditText, pw2: EditText) : Boolean{
        val patternName = Regex("^[A-Z][a-z]+(\\s[A-Z][a-z]+)*$")
        val patternPassword = Regex("^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[@$!%*?&])[A-Za-z\\d@$!%*?&]*$")

        if(name.text.toString().isEmpty() || !patternName.containsMatchIn(name.text.toString()) || (name.text.toString().length <2 || name.text.toString().length>=30)) {
            name.error = "Inserire nome valido (Deve iniziare con una lettere maiuscola e può contenere lettere e spazi"
            name.requestFocus()
            return false
        }

        if(surname.text.toString().isEmpty() || !patternName.containsMatchIn(surname.text.toString()) || surname.text.toString().length >=30){
            surname.error = "Inserire cognome valido (Deve iniziare con una lettere maiuscola e può contenere lettere e spazi"
            surname.requestFocus()
            return false
        }

        if(email.text.toString().isEmpty() || !Patterns.EMAIL_ADDRESS.matcher(email.text.toString()).matches()){
            email.error = "Inserire mail valida"
            email.requestFocus()
            return false
        }

        if(pw1.text.toString().isEmpty() || !patternPassword.containsMatchIn(pw1.text.toString()) || pw1.text.toString().length < 8){
            pw1.error = "Inserire password valida!\n" +
                    "Deve contenere almeno una lettere minuscola, una lettera maiuscuola e un carattere speciale (@$!%*?&). Lunghezza minina 8 caratteri"
            pw1.requestFocus()
            return false
        }

        if(pw1.text.toString() != pw2.text.toString()){
            pw2.error = "Le password devono essere le stesse"
            pw2.requestFocus()
            return false
        }

        return true
    }

    fun controllaOggettiScaduti():LiveData<EventWrapper<NotificaOggetto>> {
        caricamento.value = true
        val dataOdierna = Calendar.getInstance().time
        dataOdierna.hours = 0
        dataOdierna.minutes = 0
        dataOdierna.seconds = 0
        successScaduti= repository.controllaOggettiScaduti(dataOdierna)
        return successScaduti
    }

    fun logout() {
        repository.logout()
    }

    /*GETTER*/
    fun getCaricamento() : LiveData<Boolean> = caricamento

    fun getSuccessCurrent() : LiveData<EventWrapper<Boolean>>? {
        return if(this::successCurrent.isInitialized)
            successCurrent
        else
            null
    }

    fun getSuccessLogin() : LiveData<EventWrapper<Boolean>>? {
        return if(this::successLogin.isInitialized)
            successLogin
        else
            null
    }

    fun getSuccessReset() : LiveData<EventWrapper<Boolean>>? {
        return if(this::successReset.isInitialized)
            successReset
        else
            null
    }

    fun getsuccessAggiornamentoUtente() : LiveData<EventWrapper<Utente>>? {
        return if(this::successAggiornamentoUtente.isInitialized)
            successAggiornamentoUtente
        else
            null
    }

    fun getSuccessRegistrazione() : LiveData<EventWrapper<Boolean>>? {
        return if(this::successRegistration.isInitialized)
            successRegistration
        else
            null
    }

    fun getControlloOggettiScaduti(): LiveData<EventWrapper<NotificaOggetto>>? {
        return if(this::successScaduti.isInitialized)
            return successScaduti
        else
            null
    }

    /*SETTER*/
    fun setCaricamento(caricamento : Boolean) {
        this.caricamento.value = caricamento
    }
}