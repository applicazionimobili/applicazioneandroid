package com.example.stuffexchange.viewmodel

import androidx.lifecycle.ViewModel

class MainActivityViewModel: ViewModel() {

    private  var nome: String? = null
    private var email: String? = null
    private  var votoMedio: String? = null

    fun getNome(): String? = nome
    fun getEmail(): String? = email
    fun getVotoMedio(): String? = votoMedio

    fun setNome(nome: String){
        this.nome = nome
    }

    fun setEmail(email: String){
        this.email = email
    }

    fun setVotoMedio(votoMedio: String){
        this.votoMedio = votoMedio
    }
}