package com.example.stuffexchange.util

import android.Manifest.permission.ACCESS_COARSE_LOCATION
import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.*
import android.os.Bundle
import android.provider.Settings
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat

class Localizzazione(private val activity: Activity) {


    fun geolocalizzazioneAttiva(): Boolean {
        val locationManager = activity.getSystemService(Context.LOCATION_SERVICE) as LocationManager
        var gpsAttivo = false
        var reteAttiva = false

        try {
            gpsAttivo = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)
        } catch (e: Exception){

        }

        try {
           reteAttiva = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)
        } catch (e: Exception){

        }

        if(!gpsAttivo && !reteAttiva)
            return false

        return true
    }

    fun dialogPosizione(){
        AlertDialog.Builder(activity)
            .setMessage("GPS non ativo")
            .setPositiveButton("Apri le impostazioni"){_,_ ->
                val intent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                ContextCompat.startActivity(activity, intent, null)
            }
            .setNegativeButton("Annulla", null)
            .show()
    }
}


