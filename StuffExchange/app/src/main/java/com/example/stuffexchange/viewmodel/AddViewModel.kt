package com.example.stuffexchange.viewmodel

import android.annotation.SuppressLint
import android.app.Application
import android.location.Address
import android.location.Geocoder
import android.location.LocationManager
import android.net.Uri
import android.widget.EditText
import android.widget.TextView
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.stuffexchange.model.EventWrapper
import com.example.stuffexchange.model.Oggetto
import com.example.stuffexchange.repository.AddRepository
import com.example.stuffexchange.util.CallbackPosizione
import java.io.IOException
import java.lang.reflect.InvocationTargetException
import java.text.SimpleDateFormat
import java.util.*

@Suppress("DEPRECATION")
class AddViewModel(application: Application, private var geocoder: Geocoder, locationManager: LocationManager) : ViewModel() {

    private val repository : AddRepository = AddRepository(application, geocoder, locationManager)

    private var listaKeyword = MutableLiveData<String>()
    private var message = MutableLiveData("noerror")
    private var caricamento = MutableLiveData(false)
    private lateinit var caricaOggettoSuccess: LiveData<EventWrapper<Boolean>>

    private var imgUri : Uri? = null
    private var startDate :String? = null
    private var endDate: String? = null
    private var keywords :String = " "
    private var calendario = Calendar.getInstance()
    private var giorno = 0
    private var mese = 0
    private var anno = 0

    fun setCaricamento(caricamento: Boolean) {
        this.caricamento.value = caricamento
    }

    fun setImageUri(imgUri: Uri?) {
        this.imgUri = imgUri
    }

    fun setStartDate(startDate: String?) {
        this.startDate = startDate
    }

    fun setEndDate(endDate: String?) {
        this.endDate = endDate
    }

    fun getStartDate() : String? = startDate
    fun getEndDate() : String? = endDate
    fun getImageUri() : Uri? = imgUri
    fun getGiorno():Int = giorno
    fun getMese():Int = mese
    fun getAnno():Int = anno
    fun getListaKeyoword(): LiveData<String> = listaKeyword
    fun getMessage(): LiveData<String> = message
    fun getCaricamento() : LiveData<Boolean> = caricamento
    fun getCaricaOggettoSuccess(): LiveData<EventWrapper<Boolean>>?{
        return if (this::caricaOggettoSuccess.isInitialized)
            caricaOggettoSuccess
        else
            null
    }

    fun caricaOggetto(
        nomeOggetto: EditText, descrizioneOggetto: EditText, posizione: TextView, startDate: TextView, endDate: TextView):  LiveData<EventWrapper<Boolean>>? {

        caricamento.value = true
        val geoMatches: List<Address>?

        try {
            geoMatches = geocoder.getFromLocationName(posizione.text.toString(), 1)
        }catch (e: IOException){
            e.printStackTrace()
            return null
        }catch (e: InvocationTargetException){
            e.printStackTrace()
            return null
        }

        if(geoMatches != null){
            val latitudine = geoMatches[0].latitude.toFloat()
            val longitudine = geoMatches[0].longitude.toFloat()

            val oggetto = Oggetto(
                " ",
                " ",
                " ",
                " ",
                nomeOggetto.text.toString(),
                keywords,
                descrizioneOggetto.text.toString(),
                posizione.text.toString(),
                startDate.text.toString(),
                endDate.text.toString(),
                "in progress",
                latitudine,
                longitudine
            )

            caricaOggettoSuccess = repository.caricaOggetto(oggetto, imgUri)
            return caricaOggettoSuccess
        }
        else
            return null
    }

    fun selezionaData(data: String) {
        if(data.isEmpty()) {
            giorno = calendario.get(Calendar.DAY_OF_MONTH)
            mese = calendario.get(Calendar.MONTH) + 1
            anno = calendario.get(Calendar.YEAR)
        }
        else{
            val aux = data.split("/")
            giorno = aux[0].toInt()
            mese = aux[1].toInt()
            anno = aux[2].toInt()
        }
    }

    fun aggiungiKeywords(key: EditText) {
        val patternKeyWord = Regex("^[a-z]+(\\s[a-z]+)*$")

        if(key.text.toString().isEmpty() || !patternKeyWord.containsMatchIn(key.text.toString())) {
            key.error = "Keywords deve contenere solo lettere minuscole"
            key.requestFocus()
            return
        }

        keywords += "${key.text},"
        listaKeyword.value = keywords
    }

    fun rimuoviKeywords() {
        keywords = " "
        listaKeyword.value = keywords
    }

    fun rimuoviNomeOggetto(nome: EditText){
        if(nome.text.toString().isEmpty()){
            nome.error = "Campo nome è vuoto!"
            nome.requestFocus()
            return
        }
        nome.text.clear()
    }

    fun rimuoviDescrizione(descrizione: EditText){
        if(descrizione.text.toString().isEmpty()){
            descrizione.error = "Campo descrizione è vuoto!"
            descrizione.requestFocus()
            return
        }
        descrizione.text.clear()
    }

    @SuppressLint("SimpleDateFormat")
    fun controlloCampi(nome: EditText, descrizione: EditText, posizione: TextView, startData: TextView, endData: TextView, keywords: TextView) : Boolean
    {
        if(posizione.text.toString().isEmpty()) {
            message.value = "Aggiorna la posizione prima di procedere"
            return false
        }

        if(imgUri == null){
            message.value = "Inserisci una immagine"
            return false
        }

        if(nome.text.toString().isEmpty() || nome.text.toString().length<4){
            nome.error = "Inserire nome oggetto (Lunghezza minima 4)"
            nome.requestFocus()
            return false
        }

        if(keywords.text.toString().isEmpty()) {
            keywords.error = "Inserire almeno una keywords"
            keywords.requestFocus()
            return false
        }

        if(descrizione.text.toString().isEmpty()){
            descrizione.error = "Inserisci una descrizione all'oggetto"
            descrizione.requestFocus()
            return false
        }

        if(startData.text.toString().isEmpty()) {
            message.value = "Seleziona una data di inizio"
            startData.error = "Errore"
            return false
        }

        if(endData.text.toString().isEmpty()) {
            message.value = "Seleziona una data di fine"
            endData.error = "Errore"
            return false
        }

        val sdf = SimpleDateFormat("dd/M/yyyy")
        val myStartDate = sdf.parse(startData.text.toString())
        myStartDate!!.hours = 23
        myStartDate.minutes = 59
        myStartDate.seconds = 58

        val myEndDate = sdf.parse(endData.text.toString())
        myEndDate!!.hours = 23
        myEndDate.minutes = 59
        myEndDate.seconds = 59

        if(myStartDate < calendario.time) {
            message.value = "Data di inzio non può iniziare prima della data attuale"
            return false
        }

        if(myEndDate < myStartDate) {
            message.value = "Data di fine non può iniziare prima della data attuale"
            return false
        }

        return true
    }

    fun richiestaAggiornamentoPosizione(callback: CallbackPosizione, precisione: Int){
        caricamento.value = true
        repository.richiestaAggiornamentoPosizione(callback, precisione)
    }
}