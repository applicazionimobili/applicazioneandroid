package com.example.stuffexchange.view.mappa

import android.annotation.SuppressLint
import android.content.Context
import android.location.Geocoder
import android.os.Bundle
import android.view.*
import android.widget.Toast
import androidx.activity.addCallback
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.example.stuffexchange.R
import com.example.stuffexchange.model.Oggetto
import com.example.stuffexchange.util.Connessione
import com.example.stuffexchange.view.MainActivity
import com.example.stuffexchange.viewmodel.MappaViewModel
import com.example.stuffexchange.viewmodel.ViewModelFactory
import com.google.android.gms.maps.*
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.karumi.dexter.Dexter
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionDeniedResponse
import com.karumi.dexter.listener.PermissionGrantedResponse
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.single.PermissionListener
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.finestra_marker.view.*
import kotlinx.android.synthetic.main.fragment_mappa.*


class MappaFragment : Fragment(), GoogleMap.OnInfoWindowClickListener {
    private lateinit var viewModelFactory: ViewModelFactory
    private lateinit var viewModel: MappaViewModel
    private lateinit var mainActivity: MainActivity
    private var mapView: MapView? = null
    private var googleMap: GoogleMap? = null
    private lateinit var connessione: Connessione

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val mView = inflater.inflate(R.layout.fragment_mappa, container, false)
        mapView = mView.findViewById(R.id.mappa)
        mapView!!.onCreate(savedInstanceState)
        mapView!!.onResume()

        try {
            MapsInitializer.initialize(requireActivity().application)
        }catch (e: Exception){
            e.printStackTrace()
        }

        mapView?.getMapAsync(object: OnMapReadyCallback{
            override fun onMapReady(p0: GoogleMap?) {
                googleMap = p0
                googleMap?.isMyLocationEnabled = true

                val mapSettings = googleMap?.uiSettings
                mapSettings?.isZoomControlsEnabled = true
                mapSettings?.isZoomGesturesEnabled = true
                mapSettings?.isScrollGesturesEnabled = true
                mapSettings?.isTiltGesturesEnabled = true
                mapSettings?.isRotateGesturesEnabled = true

                if(!connessione.controlloConnessione()){
                    Toast.makeText(requireActivity(), "Connessione Internet assente", Toast.LENGTH_SHORT).show()
                }
                else {
                    viewModel.getLatLng().observe(viewLifecycleOwner, Observer {
                        googleMap?.moveCamera(CameraUpdateFactory.newLatLngZoom(it, 12.0f))
                    })

                    viewModel.getListaOggetti().observe(viewLifecycleOwner, Observer {
                        it?.let { arrayOggetto ->
                            arrayOggetto.forEach { oggetto ->
                                val latitude: Double = oggetto.latitudine.toDouble()
                                val longitudine: Double = oggetto.longitudine.toDouble()
                                googleMap?.addMarker(MarkerOptions().position(LatLng(latitude, longitudine)).title(oggetto.nome))?.tag = oggetto

                                googleMap?.setInfoWindowAdapter(object: GoogleMap.InfoWindowAdapter{
                                    @SuppressLint("InflateParams")
                                    override fun getInfoContents(p0: Marker?): View? {
                                        val view = requireActivity().layoutInflater.inflate(R.layout.finestra_marker, null)
                                        view.nome.text = oggetto.nome
                                        view.posizione.text = oggetto.posizione
                                        view.utente.text = oggetto.nomeVenditore
                                        return view
                                    }

                                    override fun getInfoWindow(p0: Marker?): View? {
                                        return null
                                    }

                                })
                            }

                            googleMap?.setOnInfoWindowClickListener(this@MappaFragment)
                        }
                    })

                    arguments?.let {
                        val arrayOggetti = MappaFragmentArgs.fromBundle(it).arrayOggetto
                        val posizione = MappaFragmentArgs.fromBundle(it).posizione

                        viewModel.aggiornaDatiMappa(arrayOggetti, posizione, Geocoder(requireActivity()))
                    }
                }

            }

        })



        viewModelFactory = ViewModelFactory(requireActivity().application)
        viewModel = ViewModelProvider(this, viewModelFactory).get(MappaViewModel::class.java)
        connessione = Connessione(requireActivity())
        mainActivity = requireActivity() as MainActivity


        requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner){
            val navController = findNavController()

            if(navController.currentDestination?.id == R.id.mappaFragment)
                navController.navigate(R.id.action_mappaFragment_to_searchFragment)
        }

        return mView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mainActivity.toolbar.title = "Mappa"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onResume() {
        super.onResume()
        mapView?.onResume()
    }

    override fun onPause() {
        super.onPause()
        mapView?.onPause()
    }

    override fun onDestroy() {
        super.onDestroy()
        mapView?.onDestroy()
    }

    override fun onLowMemory() {
        super.onLowMemory()
        mapView?.onLowMemory()
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_mappa, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when(item.itemId){
            R.id.listaMappaFragment->{
                val lista = viewModel.getListaOggetti().value
                lista.let {
                    val array = arrayOfNulls<Oggetto>(lista!!.size)
                    lista.toArray(array)
                    val action = MappaFragmentDirections.actionMappaFragmentToListaMappaFragment(array)
                    val navController = findNavController()

                    if(navController.currentDestination?.id == R.id.mappaFragment)
                        navController.navigate(action)
                }
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onInfoWindowClick(marker: Marker?) {
        val oggetto = marker?.tag as Oggetto
        val action = MappaFragmentDirections.actionMappaFragmentToMappaDettagliFragment(oggetto)
        val navController = findNavController()

        if(navController.currentDestination?.id == R.id.mappaFragment)
            navController.navigate(action)
    }

}
