package com.example.stuffexchange.view.list

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.activity.addCallback
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.example.stuffexchange.R
import com.example.stuffexchange.util.Caricamento
import com.example.stuffexchange.util.Connessione
import com.example.stuffexchange.view.MainActivity
import com.example.stuffexchange.view.adapter.OggettoAttivoAdapter
import com.example.stuffexchange.viewmodel.OggettiViewModel
import com.example.stuffexchange.viewmodel.ViewModelFactory
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_oggetti_presi_in_prestito.*
import kotlinx.android.synthetic.main.fragment_oggetti_presi_in_prestito.swipe
import kotlinx.android.synthetic.main.fragment_oggetti_presi_in_prestito.textEmpty


@Suppress("DEPRECATION")
class OggettiPresiInPrestitoFragment : Fragment(),  SwipeRefreshLayout.OnRefreshListener {

    private lateinit var viewModelFactory: ViewModelFactory
    private lateinit var viewModel: OggettiViewModel
    private var oggettoAttivoAdapter = OggettoAttivoAdapter(arrayListOf(), this)
    private lateinit var connessione : Connessione
    private lateinit var caricamento : Caricamento
    private lateinit var mainActivity: MainActivity

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        viewModelFactory = ViewModelFactory(requireActivity().application)
        viewModel = ViewModelProvider(this, viewModelFactory).get(OggettiViewModel::class.java)
        connessione = Connessione(requireActivity())
        caricamento = Caricamento(requireActivity())
        mainActivity = requireActivity() as MainActivity

        return inflater.inflate(R.layout.fragment_oggetti_presi_in_prestito, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mainActivity.toolbar.title = "Oggetti presi in prestito"

        lista.apply {
            layoutManager = LinearLayoutManager(requireActivity())
            adapter = oggettoAttivoAdapter
        }

        swipe.setColorSchemeColors(requireActivity().resources.getColor(R.color.colorAccent))
        swipe.setOnRefreshListener(this)

        requireActivity().onBackPressedDispatcher.addCallback(this) {
            val navController = findNavController()

            if(navController.currentDestination?.id == R.id.oggettiPresiInPrestitoFragment)
                navController.navigate(R.id.action_oggettiPresiInPrestitoFragment_to_searchFragment)
        }
    }

    override fun onResume() {
        super.onResume()
        aggiornaListaInPrestito()
    }

    override fun onStop() {
        super.onStop()
        caricamento.dismissDialog()
    }

    private fun aggiornaListaInPrestito() {
        if(!connessione.controlloConnessione()){
            Toast.makeText(requireActivity(), "Connessione Internet assente", Toast.LENGTH_SHORT).show()
            textEmpty.visibility = View.VISIBLE
            lista.visibility = View.GONE
            return
        }

        caricamento.loadingAlertDialog()

        viewModel.aggiornaListaInPrestito().observe(viewLifecycleOwner, Observer {
            it?.let {
                if(it.size > 0) {
                    lista.visibility = View.VISIBLE
                    oggettoAttivoAdapter.aggiornaLista(it)
                }
                else {
                    textEmpty.visibility = View.VISIBLE
                    lista.visibility = View.GONE
                }

                swipe.isRefreshing = false
                caricamento.dismissDialog()
            }

            if(it == null) {
                Toast.makeText(requireActivity(), "Errore durante il caricamento della lista", Toast.LENGTH_SHORT).show()
                caricamento.dismissDialog()
                swipe.isRefreshing = false
            }
        })

    }

    override fun onRefresh() {
        if(!connessione.controlloConnessione()){
            Toast.makeText(requireActivity(), "Connessione Internet assente", Toast.LENGTH_SHORT).show()
            textEmpty.visibility = View.VISIBLE
            lista.visibility = View.GONE
            swipe.isRefreshing = false
        }else{
            aggiornaListaInPrestito()
            textEmpty.visibility = View.GONE
            lista.visibility = View.VISIBLE
        }
    }

}
