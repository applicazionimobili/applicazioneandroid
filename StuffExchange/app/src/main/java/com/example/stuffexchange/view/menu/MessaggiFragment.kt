package com.example.stuffexchange.view.menu

import android.os.Bundle
import android.text.InputType
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.example.stuffexchange.R
import com.example.stuffexchange.model.UtenteChat
import com.example.stuffexchange.util.Caricamento
import com.example.stuffexchange.util.Connessione
import com.example.stuffexchange.view.MainActivity
import com.example.stuffexchange.view.adapter.MessaggiAdapter
import com.example.stuffexchange.viewmodel.ChatViewModel
import com.example.stuffexchange.viewmodel.ViewModelFactory
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_messaggi.*
import kotlinx.android.synthetic.main.fragment_messaggi.swipe


@Suppress("DEPRECATION")
class MessaggiFragment : Fragment(), SwipeRefreshLayout.OnRefreshListener {

    private lateinit var viewModelFactory: ViewModelFactory
    private lateinit var viewModel: ChatViewModel
    private lateinit var caricamento : Caricamento
    private lateinit var connessione : Connessione
    private var messaggioAdapter = MessaggiAdapter(arrayListOf())
    private lateinit var mainActivity: MainActivity

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        viewModelFactory = ViewModelFactory(requireActivity().application)
        viewModel = ViewModelProvider(this, viewModelFactory).get(ChatViewModel::class.java)
        caricamento = Caricamento(requireActivity())
        connessione = Connessione(requireActivity())
        mainActivity = requireActivity() as MainActivity
        return inflater.inflate(R.layout.fragment_messaggi, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mainActivity.toolbar.title = "Messaggi"

        arguments?.let {
            viewModel.setDestinatario(MessaggiFragmentArgs.fromBundle(it).utenteChat)
        }

        listaChat.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = messaggioAdapter
        }

        inviaMessagio.setOnClickListener {
           inviaMessaggio()
        }

        swipe.setColorSchemeColors(requireActivity().resources.getColor(R.color.colorAccent))
        swipe.setOnRefreshListener(this)
    }

    override fun onResume() {
        super.onResume()
        osservaCaricamento()
        osservaInviaMessaggio()

        if(!connessione.controlloConnessione()){
            Toast.makeText(requireActivity(), "Connessione Internet assente", Toast.LENGTH_SHORT).show()
            inviaMessagio.visibility = View.GONE
            scriviMessagio.inputType = InputType.TYPE_NULL
        }
        else{
            aggiungiListener()
            inviaMessagio.visibility = View.VISIBLE
            scriviMessagio.inputType = InputType.TYPE_CLASS_TEXT
        }
    }

    override fun onPause() {
        super.onPause()
        viewModel.stopListener()
        viewModel.setListerAttivo(false)
        caricamento.dismissDialog()
    }

    private fun aggiungiListener() {
        viewModel.aggiungiListener().observe(viewLifecycleOwner, Observer { listaMessaggi->
            if(listaMessaggi != null && !viewModel.getListenerAttivo())
                viewModel.setListerAttivo(true)

            listaMessaggi?.let {
                listaChat.visibility = View.VISIBLE
                messaggioAdapter.updateObjectMessages(listaMessaggi)
            }

            if(messaggioAdapter.itemCount > 0)
                listaChat.smoothScrollToPosition(messaggioAdapter.itemCount - 1)

            if(scriviMessagio.text.isNotEmpty())
                scriviMessagio.text.clear()

            viewModel.setCaricamento(false)
        })
    }

    private fun inviaMessaggio() {
        if(!connessione.controlloConnessione()){
            Toast.makeText(requireActivity(), "Connessione Internet assente", Toast.LENGTH_SHORT).show()
            return
        }

        viewModel.inviaMessaggio(scriviMessagio).observe(viewLifecycleOwner, Observer { eventWrapper ->
            val result = eventWrapper.getContentIfNotHandled()
            azioniInviaMessaggio(result)
        })
    }

    private fun osservaInviaMessaggio() {
        viewModel.getSuccessInvio()?.observe(viewLifecycleOwner, Observer { eventWrapper ->
            val result = eventWrapper.getContentIfNotHandled()
            azioniInviaMessaggio(result)
        })
    }

    private fun azioniInviaMessaggio(result: Boolean?) {
        result?.let {
            if(it){
                if(!viewModel.getListenerAttivo())
                    aggiungiListener()
            }
            else
                Toast.makeText(requireActivity(), "Errore durante l'invio del messaggio", Toast.LENGTH_SHORT).show()

            viewModel.setCaricamento(false)
        }
    }

    private fun osservaCaricamento() {
        viewModel.getCaricamento().observe(viewLifecycleOwner, Observer {
            if(it)
                caricamento.loadingAlertDialog()
            else {
                caricamento.dismissDialog()
                swipe.isRefreshing = false
            }
        })
    }

    override fun onRefresh() {
        if(!connessione.controlloConnessione()){
            Toast.makeText(requireActivity(), "Connessione Internet assente", Toast.LENGTH_SHORT).show()
            inviaMessagio.visibility = View.GONE
            scriviMessagio.inputType = InputType.TYPE_NULL
            swipe.isRefreshing = false
        }
        else{
            aggiungiListener()
            inviaMessagio.visibility = View.VISIBLE
            scriviMessagio.inputType = InputType.TYPE_CLASS_TEXT
        }

    }
}
