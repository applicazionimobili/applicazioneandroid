package com.example.stuffexchange.view.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import androidx.recyclerview.widget.RecyclerView
import com.example.stuffexchange.R
import com.example.stuffexchange.model.OggettoAttivo
import com.example.stuffexchange.view.list.*
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.oggetto_attivo.view.*

class OggettoAttivoAdapter(private var oggetto: ArrayList<OggettoAttivo>, private var fragment: Fragment)
    : RecyclerView.Adapter<OggettoAttivoAdapter.OggettoAttivoViewHolder>() {


    inner class OggettoAttivoViewHolder(var view: View) : RecyclerView.ViewHolder(view)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): OggettoAttivoViewHolder {
        val inflater = LayoutInflater.from(parent.context)

        return when (viewType) {
            1 -> {
                val view = inflater.inflate(R.layout.oggetto_attivo_completato, parent, false)
                OggettoAttivoViewHolder(view)
            }
            2 -> {
                val view = inflater.inflate(R.layout.oggetto_attivo_scaduto, parent, false)
                OggettoAttivoViewHolder(view)
            }
            else -> {
                val view = inflater.inflate(R.layout.oggetto_attivo, parent, false)
                OggettoAttivoViewHolder(view)
            }
        }
    }

    override fun getItemCount(): Int = oggetto.size


    override fun onBindViewHolder(holder: OggettoAttivoViewHolder, position: Int) {
        holder.view.nome.text = oggetto[position].nome
        holder.view.utente.text = oggetto[position].nomeUtente
        holder.view.startDate.text = oggetto[position].inizioPrestito
        holder.view.endDate.text = oggetto[position].finePrestito
        Picasso.get().load(oggetto[position].imgUrl).fit().placeholder(R.drawable.caricamento).into(holder.view.imageView)

        holder.view.setOnClickListener {

            if(fragment is OggettiPrestatiFragment) {
                val action = OggettiPrestatiFragmentDirections.actionOggettiPrestatiFragmentToOggettoAttivoDettagliFragment(oggetto[position],1)
                Navigation.findNavController(it).navigate(action)
            }
            else {
                val action = OggettiPresiInPrestitoFragmentDirections.actionOggettiPresiInPrestitoFragmentToOggettoAttivoDettagliFragment(oggetto[position],2)
                Navigation.findNavController(it).navigate(action)
            }

        }
    }


    fun aggiornaLista(newObjectLoan: List<OggettoAttivo>) {
        oggetto.clear()
        oggetto.addAll(newObjectLoan)
        notifyDataSetChanged()
    }


    override fun getItemViewType(position: Int): Int {

        return when (oggetto[position].stato) {
            "complete" -> 1
            "expired" -> 2
            else -> 3
        }

    }
}