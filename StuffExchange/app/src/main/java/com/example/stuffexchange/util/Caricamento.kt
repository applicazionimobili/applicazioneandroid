package com.example.stuffexchange.util

import android.annotation.SuppressLint
import android.app.Activity
import android.app.AlertDialog
import android.content.pm.ActivityInfo
import android.content.res.Configuration
import android.view.LayoutInflater
import com.example.stuffexchange.R

class Caricamento(private val activity: Activity) {

    private lateinit var builder: AlertDialog.Builder
    private  var dialog : AlertDialog? = null

    fun loadingAlertDialog(){
        if(dialog == null) {
            builder = AlertDialog.Builder(activity)
            val inflater: LayoutInflater = activity.layoutInflater
            builder.setView(inflater.inflate(R.layout.caricamento, null))
            builder.setCancelable(false)
            dialog = builder.create()
            dialog?.show()
        }
    }

    fun dismissDialog(){
        dialog?.dismiss()
        resetDialog()
    }

    fun getDialog() : AlertDialog? = dialog

    fun resetDialog() {
        dialog = null
    }
}