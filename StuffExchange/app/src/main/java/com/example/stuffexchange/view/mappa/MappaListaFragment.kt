package com.example.stuffexchange.view.mappa

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.stuffexchange.R
import com.example.stuffexchange.model.Oggetto
import com.example.stuffexchange.util.Connessione
import com.example.stuffexchange.view.MainActivity
import com.example.stuffexchange.view.adapter.MappaListaAdapter
import com.example.stuffexchange.viewmodel.MappaViewModel
import com.example.stuffexchange.viewmodel.ViewModelFactory
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_lista_mappa.*


class MappaListaFragment : Fragment() {

    private lateinit var viewModelFactory: ViewModelFactory
    private lateinit var viewModel: MappaViewModel
    private lateinit var connessione: Connessione
    private lateinit var mainActivity: MainActivity
    private var mappaAdapter = MappaListaAdapter(arrayListOf())


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        viewModelFactory = ViewModelFactory(requireActivity().application)
        viewModel = ViewModelProvider(this, viewModelFactory).get(MappaViewModel::class.java)
        connessione = Connessione(requireActivity())
        mainActivity = requireActivity() as MainActivity
        return inflater.inflate(R.layout.fragment_lista_mappa, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mainActivity.toolbar.title = "Lista oggetti"

        arguments?.let {
            val arrayOggetto = MappaListaFragmentArgs.fromBundle(it).arrayOggetto
            aggiornaLista(arrayOggetto)
        }

        lista.apply {
            layoutManager = LinearLayoutManager(requireActivity())
            adapter = mappaAdapter
        }
    }

    private fun aggiornaLista(array: Array<Oggetto>) {
        if(!connessione.controlloConnessione()){
            Toast.makeText(requireActivity(), "Connessione Internet assente", Toast.LENGTH_SHORT).show()
            return
        }

        viewModel.aggiornaLista(array).observe(viewLifecycleOwner, Observer {
            it?.let {
                if(it.size>0) {
                    lista.visibility = View.VISIBLE
                    mappaAdapter.aggiornaLista(it)
                }
            }
        })
    }
}
