package com.example.stuffexchange.view.mappa

import android.app.DatePickerDialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.example.stuffexchange.R
import com.example.stuffexchange.model.Oggetto
import com.example.stuffexchange.model.UtenteChat
import com.example.stuffexchange.util.Caricamento
import com.example.stuffexchange.util.Connessione
import com.example.stuffexchange.view.MainActivity
import com.example.stuffexchange.viewmodel.MappaViewModel
import com.example.stuffexchange.viewmodel.ViewModelFactory
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_mappa_dettagli.*


class MappaDettagliFragment : Fragment(), SwipeRefreshLayout.OnRefreshListener {

    private lateinit var viewModelFactory: ViewModelFactory
    private lateinit var viewModel: MappaViewModel
    private lateinit var connessione: Connessione
    private lateinit var caricamento : Caricamento
    private lateinit var mainActivity: MainActivity

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        viewModelFactory = ViewModelFactory(requireActivity().application)
        viewModel = ViewModelProvider(this, viewModelFactory).get(MappaViewModel::class.java)
        caricamento = Caricamento(requireActivity())
        connessione = Connessione(requireActivity())
        mainActivity = requireActivity() as MainActivity
        return inflater.inflate(R.layout.fragment_mappa_dettagli, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mainActivity.toolbar.title = "Dettaglio oggetto"

        swipe.setColorSchemeColors(requireActivity().resources.getColor(R.color.colorAccent))
        swipe.setOnRefreshListener(this)

        arguments?.let {
            val oggetto = MappaDettagliFragmentArgs.fromBundle(it).oggetto
            aggiornaDettaglio(oggetto)
        }

        startDate.setOnClickListener {
            selezionaData(startDate)
        }

        endDate.setOnClickListener {
            selezionaData(endDate)
        }

        confermo.setOnClickListener {
            aggiornaDatePrenotate(true)
        }

        bottoneChat.setOnClickListener {
            val oggetto = viewModel.getOggetto().value
            val utenteChat = UtenteChat(oggetto?.nomeVenditore, oggetto?.idVenditore)
            val action = MappaDettagliFragmentDirections.actionMappaDettagliFragmentToMessaggiFragment(utenteChat)
            findNavController().navigate(action)
        }

    }

    override fun onResume() {
        super.onResume()
        osservaCaricamento()
        osservaMessaggio()
        aggiornaDatePrenotate(false)
        osservaPrenotaOggetto()

        viewModel.getStartDate()?.let {
            startDate.text = viewModel.getStartDate()
        }

        viewModel.getEndDate()?.let {
            endDate.text = viewModel.getEndDate()
        }
    }

    override fun onStop() {
        super.onStop()
        viewModel.setStartDate(startDate.text.toString())
        viewModel.setEndDate(endDate.text.toString())
        caricamento.dismissDialog()
    }

    private fun selezionaData(dataText: TextView?) {
        viewModel.selezionaData(dataText)

        DatePickerDialog(requireActivity(), DatePickerDialog.OnDateSetListener{_, anno, mese, giorno ->
            val aux = "$giorno/${mese+1}/$anno"
            dataText?.text = aux
            dataText?.error = null
        }, viewModel.getAnno(), viewModel.getMese()-1, viewModel.getGiorno()).show()
    }

    private fun aggiornaDettaglio(oggetto: Oggetto) {
        if(!connessione.controlloConnessione()){
            Toast.makeText(requireActivity(), "Connessione internet assente", Toast.LENGTH_SHORT).show()
            return
        }

        viewModel.aggiornaDettaglio(oggetto).observe(viewLifecycleOwner, Observer {
            it?.let {
                nome.text = it.nome
                posizione.text = it.posizione
                descrizione.text = it.descrizione
                keywords.text = it.keywords
                utente.text = it.nomeVenditore
                Picasso.get().load(oggetto.imgUrl).fit().placeholder(R.drawable.caricamento).into(imageView)
                val data = "${it.startData} - ${it.endData}"
                disponibilitàDate.text = data

            }
        })
    }

    private fun aggiornaDatePrenotate(prenota : Boolean) {
        viewModel.aggiornaDatePrenotate().observe(viewLifecycleOwner, Observer { listaDate->
            listaDate?.let {
                var aux = ""

                it.forEach {parola->
                    aux += parola + "\n"
                }

                datePrenotate.text = aux

                if(prenota)
                    prenotaOggetto(startDate, endDate)
            }
        })
    }

    private fun prenotaOggetto(startDate: TextView, endDate: TextView) {
        if(!connessione.controlloConnessione()){
            Toast.makeText(requireActivity(), "Connessione Internet assente", Toast.LENGTH_SHORT).show()
            return
        }

        if(!viewModel.controlloDatiPrenotazione(startDate.text.toString(), endDate.text.toString()))
            return

        viewModel.prenotaOggetto(startDate, endDate)?.observe(viewLifecycleOwner, Observer { eventWrapper ->
            val result = eventWrapper.getContentIfNotHandled()
            azioniPrenotaOggetto(result)
        })
    }

    private fun osservaPrenotaOggetto() {
        viewModel.getSuccessPrenotazione()?.observe(viewLifecycleOwner, Observer { eventWrapper ->
            val result = eventWrapper.getContentIfNotHandled()
            azioniPrenotaOggetto(result)
        })
    }

    private fun azioniPrenotaOggetto(result: Boolean?) {
        result?.let {
            if(it) {
                Toast.makeText(requireActivity(), "Prenotazione effettuata con successo", Toast.LENGTH_SHORT).show()
                val action = MappaDettagliFragmentDirections.actionMappaDettagliFragmentToCaricamentoCompletato(3)
                findNavController().navigate(action)
                viewModel.setCaricamento(false)
            }
            else {
                viewModel.setCaricamento(false)
                Toast.makeText(requireActivity(), "Errore durante la prenotazione", Toast.LENGTH_SHORT).show()
                aggiornaDatePrenotate(false)
            }


        }
    }

    private fun osservaCaricamento() {
        viewModel.getCaricamento().observe(viewLifecycleOwner, Observer {
            if(it)
                caricamento.loadingAlertDialog()
            else
                caricamento.dismissDialog()
        })
    }

    private fun osservaMessaggio() {
        viewModel.getMessaggio().observe(viewLifecycleOwner, Observer {
            it?.let {
                Toast.makeText(requireActivity(), it, Toast.LENGTH_SHORT).show()
            }
        })
    }

    override fun onRefresh() {
        aggiornaDatePrenotate(false)
        swipe.isRefreshing = false
    }
}
