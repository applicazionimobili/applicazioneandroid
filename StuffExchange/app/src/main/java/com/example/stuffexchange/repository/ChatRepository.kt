package com.example.stuffexchange.repository

import android.app.Application
import android.os.AsyncTask
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.stuffexchange.data.FirebaseDatabase
import com.example.stuffexchange.data.LocalDatabase
import com.example.stuffexchange.model.*
import com.google.android.gms.tasks.OnFailureListener
import com.google.android.gms.tasks.OnSuccessListener
import com.google.common.base.Ascii.toLowerCase
import com.google.firebase.firestore.QuerySnapshot

class ChatRepository(application: Application) {
    private val firebaseDatabase = FirebaseDatabase.instance
    private val localDatabase = LocalDatabase.getInstance(application)
    private var utenteLoggato: Utente? = null

    init {
        UtenteAsyncTask(localDatabase, this).execute()
    }

    /*Recupera l'utente dal database locale e lo salva nella variabile utenteLoggato*/
    class UtenteAsyncTask(private val localDatabase: LocalDatabase, private val chatRepository: ChatRepository) : AsyncTask<Void, Void, Utente>() {
        override fun doInBackground(vararg params: Void?): Utente {
            return localDatabase.getUtenteDAO().getUtente()
        }

        override fun onPostExecute(result: Utente?) {
            super.onPostExecute(result)
            chatRepository.utenteLoggato = result
        }
    }

    /*Vengono selezionati tutti gli utenti tranne quello loggato*/
    fun aggiornaListaChat(filtro: String?): LiveData<ArrayList<UtenteChat>> {
        val listaUtenti = MutableLiveData<ArrayList<UtenteChat>>()

        val listaUtentiSuccessListener = OnSuccessListener<QuerySnapshot>{ document ->
            val arrayList = ArrayList<UtenteChat>()

            utenteLoggato?.let { utenteLoggato ->
                document?.forEach {
                    val chat = it.toObject(Chat::class.java)
                    if(it.id.contains(utenteLoggato.id)) {
                        if(filtro == null){
                            if(utenteLoggato.id == chat.idMittente)
                                arrayList.add(UtenteChat(chat.nomeDestinatario, chat.idDestinatario))
                            else
                                arrayList.add(UtenteChat(chat.nomeMittente, chat.idMittente))
                        }
                        else{
                            if(utenteLoggato.id == chat.idMittente && toLowerCase(chat.nomeDestinatario).contains(toLowerCase(filtro)))
                                arrayList.add(UtenteChat(chat.nomeDestinatario, chat.idDestinatario))
                            else if(utenteLoggato.id == chat.idDestinatario && toLowerCase(chat.nomeMittente).contains(toLowerCase(filtro)))
                                arrayList.add(UtenteChat(chat.nomeMittente, chat.idMittente))
                        }
                    }
                }
            }

            listaUtenti.value = arrayList
        }

        val listaUtentiFailureListener = OnFailureListener {
            listaUtenti.value = null
        }

        firebaseDatabase.aggiornaListaChat(listaUtentiSuccessListener, listaUtentiFailureListener)
        return listaUtenti
    }

    /*Ricerca fra tutte le chat quella desiderata e se la trova aggiunge un listener per ascoltarne
    * i cambiamenti*/
    fun aggiungiListener(destinatario: UtenteChat): LiveData<ArrayList<Messaggio>> {
        val listaMessaggi = MutableLiveData<ArrayList<Messaggio>>()

        val recuperaUtenteSuccessListener = OnSuccessListener<QuerySnapshot>{document->
            utenteLoggato.let {
                var trovato = false

                document?.forEach {
                    val chat = it.toObject(Chat::class.java)
                    if(chat.idChat.contains(utenteLoggato!!.id) && chat.idChat.contains(destinatario.id!!)){
                        trovato = true
                        firebaseDatabase.aggiungiListener(it.id, listaMessaggi)
                    }
                }

                if(!trovato)
                    listaMessaggi.value = null
            }
        }

        val recuperaUtenteFailureListener = OnFailureListener {
            listaMessaggi.value = null
        }

        /*Restituisce tutte le chat*/
        firebaseDatabase.getChat(recuperaUtenteSuccessListener, recuperaUtenteFailureListener)

        return listaMessaggi
    }

    /*Ricerca la chat desideta e se la trova invia il messaggio su quella chat. Se non la trova
    * crea la chat e invia il messaggio.*/
    fun inviaMessaggio(messaggio: Messaggio, destinatario: UtenteChat): LiveData<EventWrapper<Boolean>> {
        val success = MutableLiveData<EventWrapper<Boolean>>()

        val inviaMessaggioSuccessListener = OnSuccessListener<Void>{
            success.value = EventWrapper(true)
        }

        val inviaMessaggioFailureListener = OnFailureListener {
            success.value = EventWrapper(false)
        }

        val mittente = utenteLoggato

        val recuperaUtenteSuccessListener = OnSuccessListener<QuerySnapshot> { document ->
            mittente?.let {
                var trovato = false
                val nomeMittente = "${mittente.nome} ${mittente.cognome}"
                messaggio.idMittente = mittente.id
                messaggio.nomeMittente = nomeMittente

                document?.forEach {
                    val chat = it.toObject(Chat::class.java)
                    if(chat.idChat.contains(mittente.id) && chat.idChat.contains(destinatario.id!!)){
                        trovato = true
                        firebaseDatabase.inviaMessaggio(it.id, messaggio, inviaMessaggioSuccessListener, inviaMessaggioFailureListener)
                    }
                }

                if(!trovato){
                    val chat = Chat("${mittente.id}_${destinatario.id!!}", mittente.id, nomeMittente, destinatario.id!!, destinatario.nome!!, 0)
                    firebaseDatabase.creaChat(chat, messaggio, inviaMessaggioSuccessListener, inviaMessaggioFailureListener)
                }
            }
        }

        val recuperaUtenteFailureListener = OnFailureListener {
            success.value = EventWrapper(false)
        }

        firebaseDatabase.getChat(recuperaUtenteSuccessListener, recuperaUtenteFailureListener)

        return success
    }

    fun stopListener() {
        firebaseDatabase.stopListener()
    }
}