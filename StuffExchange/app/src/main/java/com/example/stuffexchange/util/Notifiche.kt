package com.example.stuffexchange.util

import android.app.Activity
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.os.Build
import androidx.core.app.NotificationCompat
import com.example.stuffexchange.R

class Notifiche(private val activity: Activity) {

    private var notificationManager : NotificationManager? = null

    fun setupNotification(){
        notificationManager = activity.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager?
        createNotificationChannel("StuffExchange","Oggetto scaduto","Il tuo oggetto è scaduto")
    }

    private fun createNotificationChannel(id: String, name: String, description: String) {

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val importance = NotificationManager.IMPORTANCE_LOW
            val channel = NotificationChannel(id, name, importance)

            channel.description = description

            notificationManager?.createNotificationChannel(channel)
        }
    }

    fun sendNotification(message: String, numero: Int) {
        var notificationID = 101

        when(numero){
            1->{
                notificationID = 101
            }

            2->{
                notificationID = 102
            }

            3->{
                notificationID = 103
            }
        }


        val channelID = "StuffExchange"
        val notification = NotificationCompat.Builder(
                activity,
                channelID
            )
            .setContentTitle("Oggetto scaduto!")
            .setContentText(message)
            .setSmallIcon(R.drawable.ic_oggetti)
            .setChannelId(channelID)
            .build()
        notificationManager?.notify(notificationID, notification)

    }
}