package com.example.stuffexchange.view.adapter

import android.opengl.Visibility
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.Navigation
import androidx.recyclerview.widget.RecyclerView
import com.example.stuffexchange.R
import com.example.stuffexchange.model.Chat
import com.example.stuffexchange.model.Utente
import com.example.stuffexchange.model.UtenteChat
import com.example.stuffexchange.view.menu.ChatFragmentDirections
import kotlinx.android.synthetic.main.utente_chat.view.*

class ChatAdapter(private var chatList: ArrayList<UtenteChat>) : RecyclerView.Adapter<ChatAdapter.ChatAdapterViewHolder>() {

    inner class ChatAdapterViewHolder(var view: View) : RecyclerView.ViewHolder(view)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ChatAdapterViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(R.layout.utente_chat, parent, false)
        return ChatAdapterViewHolder(view)
    }

    override fun getItemCount(): Int = chatList.size

    override fun onBindViewHolder(holder: ChatAdapterViewHolder, position: Int) {
        holder.view.nome.text = chatList[position].nome


       holder.view.setOnClickListener {
            val action = ChatFragmentDirections.actionChatFragmentToMessaggiFragment(chatList[position])
            Navigation.findNavController(it).navigate(action)
        }
    }


    fun aggiornaLista(newListChat: ArrayList<UtenteChat>) {
        chatList.clear()
        chatList.addAll(newListChat)
        notifyDataSetChanged()
    }
}
