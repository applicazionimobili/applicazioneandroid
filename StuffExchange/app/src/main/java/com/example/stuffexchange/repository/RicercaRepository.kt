package com.example.stuffexchange.repository

import android.Manifest
import android.annotation.SuppressLint
import android.app.Application
import android.content.pm.PackageManager
import android.location.*
import android.os.AsyncTask
import android.os.Bundle
import androidx.core.app.ActivityCompat
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.stuffexchange.data.FirebaseDatabase
import com.example.stuffexchange.data.LocalDatabase
import com.example.stuffexchange.model.EventWrapper
import com.example.stuffexchange.model.Oggetto
import com.example.stuffexchange.model.Utente
import com.example.stuffexchange.util.CallbackPosizione
import com.google.android.gms.tasks.OnFailureListener
import com.google.android.gms.tasks.OnSuccessListener
import com.google.common.base.Ascii.toLowerCase
import com.google.firebase.firestore.QuerySnapshot
import java.io.IOException
import java.lang.reflect.InvocationTargetException
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

@Suppress("DEPRECATION")
class RicercaRepository(private val application: Application, private val locationManager: LocationManager, private val geocoder: Geocoder) {
    private val firebaseDatabase = FirebaseDatabase.instance
    private val localeDatabase = LocalDatabase.getInstance(application)
    private var utenteLoggato: Utente? = null

    init {
        UtenteAsyncTask(localeDatabase, this).execute()
    }

    /*Recuperare l'utente dal database locale e lo assegna a utenteLoggato*/
    class UtenteAsyncTask(
        private val localDatabase: LocalDatabase,
        private val ricercaRepository: RicercaRepository
    ) : AsyncTask<Void, Void, Utente>() {
        override fun doInBackground(vararg params: Void?): Utente {
            return localDatabase.getUtenteDAO().getUtente()
        }

        override fun onPostExecute(result: Utente?) {
            super.onPostExecute(result)
            ricercaRepository.utenteLoggato = result
        }
    }

    @SuppressLint("SimpleDateFormat")
    fun cercaOggetti(nomeOggetto: String, latitudine: Float, longitudine: Float, distanza: Int): LiveData<EventWrapper<ArrayList<Oggetto>>> {
        val oggettiTotale = MutableLiveData<EventWrapper<ArrayList<Oggetto>>>()
        val utenti = ArrayList<Utente>()
        val oggetti = ArrayList<Oggetto>()

        val ricercaSuccessListener = OnSuccessListener<List<QuerySnapshot>> {
            oggettiTotale.value = EventWrapper(oggetti)
        }

        /*Per ogni collezzione degli oggetti caricati di ogni utente vengono selezionato solo gli oggetti
        * che fanno match con i filtri di ricerca*/
        val oggettiSuccessListener = OnSuccessListener<QuerySnapshot> { listaOggetti ->
            listaOggetti?.forEach {
                val oggetto = it.toObject(Oggetto::class.java)
                val latitudineOggetto = oggetto.latitudine
                val longitudineOggetto = oggetto.longitudine
                val sdf = SimpleDateFormat("dd/M/yyyy")
                val fineDataOggetto = sdf.parse(oggetto.endData!!)
                val giornoAttuale = Calendar.getInstance().time
                fineDataOggetto!!.hours = 1
                giornoAttuale.seconds = 0
                giornoAttuale.minutes = 0
                giornoAttuale.hours = 0


                if(latitudineOggetto >= latitudine - (distanza/10)*0.0909 && latitudineOggetto <= latitudine +(distanza/10)*0.0909)
                    if(longitudineOggetto >= longitudine - (distanza/10)*0.09009 && longitudineOggetto <= longitudine +(distanza/10)*0.09009)
                        if(toLowerCase(nomeOggetto) == toLowerCase(oggetto.nome!!) || oggetto.keywords!!.contains(toLowerCase(nomeOggetto)))
                            if(fineDataOggetto!! >= giornoAttuale)
                                oggetti.add(oggetto)
            }
        }

        val ricercaFailureListener = OnFailureListener {
            oggettiTotale.value = null
        }


        val utentiSuccessListener = OnSuccessListener<QuerySnapshot>{ listaUtenti ->
            utenteLoggato?.let { utenteLoggato ->
                listaUtenti?.forEach {
                    val utente = it.toObject(Utente::class.java)

                    /*Seleziono tutti gli utenti tranne quello loggato*/
                    if (utente.id != utenteLoggato.id)
                        utenti.add(utente)
                }

                /*Per ogni utente seleziono la collection "Oggetti caricati"*/
                firebaseDatabase.cercaOggetti(utenti, oggettiSuccessListener, ricercaSuccessListener, ricercaFailureListener)
            }
        }

        /*Restituisce la lista degli utenti*/
        firebaseDatabase.aggiornaListaUtenti(utentiSuccessListener, ricercaFailureListener)
        return oggettiTotale
    }

    fun richiestaAggiornamentoPosizione(callback: CallbackPosizione, precisione: Int) {
        val criteria = Criteria()
        criteria.accuracy = precisione

        if (ActivityCompat.checkSelfPermission(application, android.Manifest.permission.ACCESS_FINE_LOCATION)
            != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(application,
                Manifest.permission.ACCESS_COARSE_LOCATION)
            != PackageManager.PERMISSION_GRANTED) {}

        locationManager.requestSingleUpdate(criteria,
            object : LocationListener {
                override fun onLocationChanged(location: Location?) {

                    location?.let {
                        var geoMatches : List<Address>? = null

                        try {
                            geoMatches = geocoder.getFromLocation(location.latitude, location.longitude, 1)
                        }catch(e: IOException){
                            e.printStackTrace()
                        }catch(e: InvocationTargetException){
                            e.printStackTrace()
                        }

                        if(geoMatches != null)
                            callback.onNewLocationAviable(geoMatches[0].getAddressLine(0))
                        else
                            callback.onNewLocationAviable("Posizione non trovata")
                    }
                }

                override fun onStatusChanged(provider: String?, status: Int, extras: Bundle?) {}
                override fun onProviderEnabled(provider: String?) {}
                override fun onProviderDisabled(provider: String?) {}},null)
    }
}