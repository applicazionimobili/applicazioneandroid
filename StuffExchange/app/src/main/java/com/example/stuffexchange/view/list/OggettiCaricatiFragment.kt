package com.example.stuffexchange.view.list

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.activity.addCallback
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.example.stuffexchange.R
import com.example.stuffexchange.util.Caricamento
import com.example.stuffexchange.util.Connessione
import com.example.stuffexchange.view.MainActivity
import com.example.stuffexchange.view.adapter.OggettiCaricatiAdapter
import com.example.stuffexchange.viewmodel.OggettiViewModel
import com.example.stuffexchange.viewmodel.ViewModelFactory
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_oggetti_caricati.*


@Suppress("DEPRECATION")
class OggettiCaricatiFragment : Fragment(), SwipeRefreshLayout.OnRefreshListener {

    private lateinit var caricamento: Caricamento
    private lateinit var connessione: Connessione
    private lateinit var viewModelFactory: ViewModelFactory
    private lateinit var viewModel: OggettiViewModel
    private val oggettoCaricatoAdapter = OggettiCaricatiAdapter(arrayListOf())
    private lateinit var mainActivity: MainActivity

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        viewModelFactory = ViewModelFactory(requireActivity().application)
        viewModel = ViewModelProvider(this, viewModelFactory).get(OggettiViewModel::class.java)
        caricamento = Caricamento(requireActivity())
        connessione = Connessione(requireActivity())
        mainActivity = requireActivity() as MainActivity

        return inflater.inflate(R.layout.fragment_oggetti_caricati, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mainActivity.toolbar.title = "Oggetti caricati"


        listaOggetti.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = oggettoCaricatoAdapter
        }

        swipe.setColorSchemeColors(requireActivity().resources.getColor(R.color.colorAccent))
        swipe.setOnRefreshListener(this)

        requireActivity().onBackPressedDispatcher.addCallback(this) {
            val navController = findNavController()

            if(navController.currentDestination?.id == R.id.oggettiCaricatiFragment)
                navController.navigate(R.id.action_oggettiCaricatiFragment_to_searchFragment)
        }
    }

    override fun onResume() {
        super.onResume()
        scaricaLista()
    }

    override fun onStop() {
        super.onStop()
        caricamento.dismissDialog()
    }

    private fun scaricaLista() {
        if(!connessione.controlloConnessione()){
            Toast.makeText(requireActivity(), "Connessione Internet assente", Toast.LENGTH_SHORT).show()
            return
        }

        caricamento.loadingAlertDialog()
        viewModel.scaricaListaOggettiCaricati().observe(viewLifecycleOwner, Observer {

            it?.let {
                if(it.size == 0)
                    textEmpty.visibility = View.VISIBLE
                else {
                    listaOggetti.visibility = View.VISIBLE
                    oggettoCaricatoAdapter.aggiornaLista(it)
                }
                swipe.isRefreshing = false
                caricamento.dismissDialog()
            }

            if(it == null) {
                Toast.makeText(requireActivity(), "Errore durante il caricamento della lista", Toast.LENGTH_SHORT).show()
                swipe.isRefreshing = false
                caricamento.dismissDialog()
            }
        })
    }

    override fun onRefresh() {
        if(!connessione.controlloConnessione()){
            Toast.makeText(requireActivity(), "Connessione Internet assente", Toast.LENGTH_SHORT).show()
            textEmpty.visibility = View.VISIBLE
            listaOggetti.visibility = View.GONE
            swipe.isRefreshing = false
        }else{
            scaricaLista()
            textEmpty.visibility = View.GONE
            listaOggetti.visibility = View.VISIBLE
        }
    }

}
