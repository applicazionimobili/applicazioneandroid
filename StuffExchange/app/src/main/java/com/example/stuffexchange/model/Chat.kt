package com.example.stuffexchange.model

class Chat(
    var idChat: String,
    var idMittente: String,
    var nomeMittente: String,
    var idDestinatario: String,
    var nomeDestinatario: String,
    var numeroMessaggi: Int) {

    constructor() : this("","","","","",0)
}