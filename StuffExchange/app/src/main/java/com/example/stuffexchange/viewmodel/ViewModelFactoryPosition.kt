package com.example.stuffexchange.viewmodel

import android.app.Application
import android.location.Geocoder
import android.location.LocationManager
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class ViewModelFactoryPosition(private val application: Application,
                               private val geocoder: Geocoder,
                               private val locationManager: LocationManager) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return modelClass.getConstructor(
            Application::class.java,
            Geocoder::class.java,
            LocationManager::class.java).newInstance(application, geocoder, locationManager)
    }
}