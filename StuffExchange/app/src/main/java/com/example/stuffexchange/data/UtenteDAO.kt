package com.example.stuffexchange.data

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.stuffexchange.model.Utente

@Dao
interface UtenteDAO {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertUtente(vararg utente: Utente) :List<Long>

    @Query ("SELECT * FROM Utente")
    fun getUtente(): Utente

    @Query("UPDATE Utente SET `Numero operazioni` = `Numero operazioni` + 1")
    fun updateNumeroOperazioni()

    @Query("UPDATE Utente SET `Numero voti`= :nuovoNumeroVoti, `Voto Medio` = :nuovoVotoMedio")
    fun updateRecensione(nuovoNumeroVoti: Int, nuovoVotoMedio: Float)

}