package com.example.stuffexchange.view.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.Navigation
import androidx.recyclerview.widget.RecyclerView
import com.example.stuffexchange.R
import com.example.stuffexchange.model.Oggetto
import com.example.stuffexchange.view.mappa.MappaListaFragmentDirections
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.oggetto_mappa.view.*


class MappaListaAdapter(private val listaOggetti: ArrayList<Oggetto>) : RecyclerView.Adapter<MappaListaAdapter.MappaListaViewHolder>() {

    inner class MappaListaViewHolder(var view: View) : RecyclerView.ViewHolder(view) {}

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MappaListaViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(R.layout.oggetto_mappa, parent, false)
        return MappaListaViewHolder((view))
    }

    override fun getItemCount(): Int = listaOggetti.size

    override fun onBindViewHolder(holder: MappaListaViewHolder, position: Int) {
        holder.view.nome.text = listaOggetti[position].nome
        holder.view.posizione.text = listaOggetti[position].posizione
        holder.view.utente.text = listaOggetti[position].nomeVenditore
        Picasso.get().load(listaOggetti[position].imgUrl).fit().placeholder(R.drawable.caricamento).into(holder.view.imageView)

        holder.view.setOnClickListener {
            val action = MappaListaFragmentDirections.actionListaMappaFragmentToMappaDettagliFragment(listaOggetti[position])
            Navigation.findNavController(it).navigate(action)
        }

    }


    fun aggiornaLista(newOggettoLista: List<Oggetto>) {
        listaOggetti.clear()
        listaOggetti.addAll(newOggettoLista)
        notifyDataSetChanged()
    }

}