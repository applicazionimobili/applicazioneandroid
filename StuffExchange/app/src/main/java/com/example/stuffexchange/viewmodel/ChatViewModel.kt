package com.example.stuffexchange.viewmodel

import android.app.Application
import android.widget.EditText
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.stuffexchange.model.*
import com.example.stuffexchange.repository.ChatRepository

class ChatViewModel(application: Application) : ViewModel() {
    private val repository = ChatRepository(application)

    private lateinit var listaUtenti : LiveData<ArrayList<UtenteChat>>
    private lateinit var listaMessaggi: LiveData<ArrayList<Messaggio>>
    private lateinit var successInvio: LiveData<EventWrapper<Boolean>>
    private val caricamento = MutableLiveData(false)

    private lateinit var listaFiltrata: ArrayList<UtenteChat>
    private lateinit var destinatario: UtenteChat
    private var listenerAttivo = false

    fun aggiornaListaChat(filtro: String?): LiveData<ArrayList<UtenteChat>> {
        caricamento.value = true

        listaUtenti = if(filtro != null)
            repository.aggiornaListaChat(filtro)
        else
            repository.aggiornaListaChat(null)

        return listaUtenti
    }

    fun getListaChat() : LiveData<ArrayList<UtenteChat>>? {
        return if(this::listaUtenti.isInitialized)
            listaUtenti
        else
            null
    }

    fun getCaricamento() : LiveData<Boolean> = caricamento

    fun setCaricamento(caricamento: Boolean) {
        this.caricamento.value = caricamento
    }


    fun aggiungiListener(): LiveData<ArrayList<Messaggio>> {
        caricamento.value = true
        listaMessaggi = repository.aggiungiListener(destinatario)
        return listaMessaggi
    }

    fun setDestinatario(destinatario: UtenteChat) {
        this.destinatario = destinatario
    }

    fun inviaMessaggio(testoMessaggio: EditText): LiveData<EventWrapper<Boolean>> {
        caricamento.value = true
        val messaggio = Messaggio("", "", "", testoMessaggio.text.toString())
        successInvio = repository.inviaMessaggio(messaggio, destinatario)
        return successInvio
    }

    fun stopListener() {
        repository.stopListener()
    }

    fun setListerAttivo(listenerAttivo: Boolean){
        this.listenerAttivo = listenerAttivo
    }

    fun getListenerAttivo(): Boolean = listenerAttivo

    fun getSuccessInvio() : LiveData<EventWrapper<Boolean>>? {
        return if(this::successInvio.isInitialized)
            successInvio
        else
            null
    }
}