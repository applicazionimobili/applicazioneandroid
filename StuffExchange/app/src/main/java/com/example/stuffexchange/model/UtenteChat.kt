package com.example.stuffexchange.model

import android.os.Parcel
import android.os.Parcelable

data class UtenteChat(
    var nome: String?,
    var id: String?
): Parcelable{
    constructor(parcel: Parcel) : this(
        parcel.readString(),
        parcel.readString()) {}

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(nome)
        parcel.writeString(id)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<UtenteChat> {
        override fun createFromParcel(parcel: Parcel): UtenteChat {
            return UtenteChat(parcel)
        }

        override fun newArray(size: Int): Array<UtenteChat?> {
            return arrayOfNulls(size)
        }
    }

}


