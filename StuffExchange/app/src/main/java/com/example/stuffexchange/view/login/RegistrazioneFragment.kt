package com.example.stuffexchange.view.login

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.example.stuffexchange.R
import com.example.stuffexchange.model.Utente
import com.example.stuffexchange.util.Caricamento
import com.example.stuffexchange.util.Connessione
import com.example.stuffexchange.view.MainActivity
import com.example.stuffexchange.viewmodel.LoginViewModel
import com.example.stuffexchange.viewmodel.ViewModelFactory
import kotlinx.android.synthetic.main.fragment_registrazione.*

class RegistrazioneFragment : Fragment() {

    private lateinit var viewModelFactory: ViewModelFactory
    private lateinit var viewModel: LoginViewModel
    private lateinit var caricamento : Caricamento
    private lateinit var connessione: Connessione
    private lateinit var mainActivity: MainActivity
    private lateinit var appCompatActivity: AppCompatActivity


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        mainActivity = requireActivity() as MainActivity
        viewModelFactory = ViewModelFactory(mainActivity.application)
        viewModel = ViewModelProvider(this, viewModelFactory).get(LoginViewModel::class.java)
        caricamento = Caricamento(requireActivity())
        connessione = Connessione(requireActivity())
        appCompatActivity = requireActivity() as AppCompatActivity
        mainActivity.setDrawerLocked(true)
        return inflater.inflate(R.layout.fragment_registrazione, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        submit.setOnClickListener {
            registrazione()
        }

        signIn.setOnClickListener {
            val navController = findNavController()

            if(navController.currentDestination?.id == R.id.registrazioneFragment)
                navController.navigate(R.id.action_registrazioneFragment_to_loginFragment)
        }
    }

    override fun onResume() {
        super.onResume()
        osservaCaricamento()
        appCompatActivity.supportActionBar?.hide()
        osservaSuccessRegistrazione()
        osservaAggiornamentoUtente()
    }

    override fun onStop() {
        super.onStop()
        caricamento.dismissDialog()
    }

    private fun registrazione() {
        if(!connessione.controlloConnessione()){
            Toast.makeText(requireActivity(), "Connessione assente", Toast.LENGTH_SHORT).show()
            return
        }

        if(!viewModel.controlloDatiRegistrazione(nome, cognome, email, password, password2))
            return

        viewModel.registrazione(nome, cognome, email, password).observe(viewLifecycleOwner, Observer { eventWrapper ->
            val result = eventWrapper.getContentIfNotHandled()
            azioniRegistrazione(result)
        })
    }

    private fun osservaSuccessRegistrazione() {
        viewModel.getSuccessRegistrazione()?.observe(viewLifecycleOwner, Observer { eventWrapper ->
            val result = eventWrapper.getContentIfNotHandled()
            azioniRegistrazione(result)
        })
    }

    private fun azioniRegistrazione(result: Boolean?) {
        result?.let {
            if (it) {
                aggiornamentoDatiUtente()
                Toast.makeText(requireActivity(), "Registrazione effettuata con successo", Toast.LENGTH_SHORT).show()
            } else {
                viewModel.setCaricamento(false)
                Toast.makeText(requireActivity(), "Errore durante la registrazione", Toast.LENGTH_SHORT).show()
            }
        }
    }

    private fun aggiornamentoDatiUtente() {
        viewModel.aggiornamentoDatiUtente()?.observe(viewLifecycleOwner, Observer { eventWrapper ->
            val result = eventWrapper.getContentIfNotHandled()
            azioniAggiornamentoUtente(result)
        })
    }

    private fun osservaAggiornamentoUtente() {
        viewModel.getsuccessAggiornamentoUtente()?.observe(viewLifecycleOwner, Observer { eventWrapper ->
            val result = eventWrapper.getContentIfNotHandled()
            azioniAggiornamentoUtente(result)
        })
    }

    private fun azioniAggiornamentoUtente(result: Utente?) {
        result?.let {
            viewModel.setCaricamento(false)
            mainActivity.aggiornamentoDrawer(it.nome, it.cognome, it.mail, it.votoMedio, it.numeroVoti)
            Toast.makeText(requireActivity(), "Dati utente aggiornati", Toast.LENGTH_SHORT).show()
            mainActivity.setDrawerLocked(false)
            val navController = findNavController()

            if(navController.currentDestination?.id == R.id.registrazioneFragment)
                navController.navigate(R.id.action_registrazioneFragment_to_searchFragment)
        }

        if(result == null){
            viewModel.logout()
            viewModel.setCaricamento(false)
            Toast.makeText(requireActivity(), "Errore durante l'aggiornamento dei dati utente, riprovare il login", Toast.LENGTH_SHORT).show()
            val navController = findNavController()

            if(navController.currentDestination?.id == R.id.registrazioneFragment)
                navController.navigate(R.id.action_registrazioneFragment_to_loginFragment)
        }
    }

    private fun osservaCaricamento() {
        viewModel.getCaricamento().observe(viewLifecycleOwner, Observer {
            if(it)
                caricamento.loadingAlertDialog()
            else
                caricamento.dismissDialog()
        })
    }
}
